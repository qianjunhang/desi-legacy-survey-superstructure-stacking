import os
import PairCountTOxi as pcxi

paircount=1
analysis=0

#os.system('cd /disk1/qhang/Projects/CorrFunc_Shadab/')

comm='python /disk1/qhang/Projects/CorrFunc_Shadab/Runme_Correlation.py -sampmode 0 '
#comm=comm+'-data /disk2/qhang/ISW_CMB/eBOSS_data/Voids-centre-vol-Rmin0-Rmax300-bins40-tol0.fits '
comm=comm+'-data /disk2/qhang/ISW_CMB/DESY1A1/Voids_M2/Results/final-voids-zslics.fits '
#comm=comm+'-rand /disk2/qhang/ISW_CMB/eBOSS_data/ELGv4-DR16LRG-DR16QSO-overlap-rand_adjusted_zdist.fits '
comm=comm+'-rand /disk2/qhang/ISW_CMB/DESY1A1/DES_Y1A1_3x2pt_redMaGiC_RANDOMS.fits '
#comm=comm+'-data2 /disk2/qhang/ISW_CMB/eBOSS_data/ELGLRGQSO_Overlap-mean-s5-g5-gdd-Combined-wP-gal_wDECALS.fits '
comm=comm+'-data2 /disk2/qhang/ISW_CMB/DESY1A1/DES_Y1A1_redMaGiC_RA_DEC_Z.fits '
#comm=comm+'-rand2 /disk2/qhang/ISW_CMB/eBOSS_data/ELGv4-DR16LRG-DR16QSO-overlap-rand_adjusted_zdist.fits '
comm=comm+'-rand2 /disk2/qhang/ISW_CMB/DESY1A1/DES_Y1A1_3x2pt_redMaGiC_RANDOMS.fits '
comm=comm+'-nbins 25 20 '
comm=comm+'-samplim 0 100 0 1 '
comm=comm+'-njn 0 '
#comm=comm+'-outfile /disk2/qhang/ISW_CMB/eBOSS_data/PairCounts/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0 '
comm=comm+'-outfile /disk2/qhang/ISW_CMB/DESY1A1/Voids_M2/Results/CrossCorr-voids-galaxies '
comm=comm+'-filetype fits '
comm=comm+'-z1z2 0.2 0.65 '
comm=comm+'-randfactor 20 '
comm=comm+'-nproc 8'

if paircount==1:
    print('Running: '+comm)
    os.system(comm)
if analysis==1:
    inroots=[]
    NJNs=[]
    root='/disk2/qhang/ISW_CMB/eBOSS_data/PairCounts/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0'
    njn=0
    xitype='crossAll'
    inroots.append(root)
    NJNs.append(njn)
    samp='rp-pi'
    xi2droot='/disk2/qhang/ISW_CMB/eBOSS_data/XI2D/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0'
    xi02root='/disk2/qhang/ISW_CMB/eBOSS_data/WP/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0'
    print(inroots,NJNs)
    pcxi.xi2d_wp_xi02_Manyfroot_jn(inroots=inroots,NJNs=NJNs,xi2droot=xi2droot,outroot=xi02root,xitype=xitype,nscomb=1,samp=samp)

