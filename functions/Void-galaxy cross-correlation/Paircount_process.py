import numpy as np

import sys
sys.path.insert(0, '~/Projects/CorrFunc_Shadab')

import PairCountTOxi as pcxi
import argparse
#Parameters goes here

parser = argparse.ArgumentParser(description='Process the pair count files:')

#these should be input
parser.add_argument('-filetype',type=int,default=0, help='0=GAMA_data, 1=LC_mocks')
parser.add_argument('-root', help='file path (same as input for Runme_Correlation.py), field or volume denoted by number d') 
parser.add_argument('-njn' ,type=int, default=18, help='Number of Jacknifes')
parser.add_argument('-xitype' ,default='auto',help='auto=auto, cross=cross(LS), crossAll=cross(D1D2/D1R2-1)')
parser.add_argument('-xi2droot' ,help='outroot for xi2d')
parser.add_argument('-xi02root' ,help='outroot for wp')
parser.add_argument('-samp' ,help='rp-pi or logrp-pi etc.')

args = parser.parse_args()

if(args.filetype==0):
    for jj in range(1,2):
        inroots = []
        NJNs = []
        for ii in range(1,2):   #fields
            root=args.root%(ii,jj)
            njn=args.njn
            xitype=args.xitype
            inroots.append(root)
            NJNs.append(njn)
            samp=args.samp

        xi2droot=args.xi2droot%jj
        xi02root=args.xi02root%jj

        print(inroots,NJNs)
        pcxi.xi2d_wp_xi02_Manyfroot_jn(inroots=inroots,NJNs=NJNs,xi2droot=xi2droot,outroot=xi02root,xitype=xitype,nscomb=1,samp=samp)

if(args.filetype==1):
    inroots = []
    NJNs = []
    for ii in range(1,4):   #fields
        for jj in range(25):   #volume
            root=args.root%(ii,jj)
            njn=args.njn
            xitype=args.xitype
            inroots.append(root)
            NJNs.append(njn)
            samp=args.samp
    xi2droot=args.xi2droot
    xi02root=args.xi02root

    print(inroots,NJNs)
    pcxi.xi2d_wp_xi02_Manyfroot_jn(inroots=inroots,NJNs=NJNs,xi2droot=xi2droot,outroot=xi02root,xitype=xitype,nscomb=1,samp=samp)
