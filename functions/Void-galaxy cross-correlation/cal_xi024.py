import sys
sys.path.insert(0, '/disk1/qhang/CorrFuncShadab')
import numpy as np
import General_function_HOD as GFHOD

def xi2Dmodel(param):
    data = np.loadtxt('/disk2/qhang/CovarianceCompare/Data/XI2D/Galauto_data-rp-pi-NJN-54.txt')
    xi = data[:,2].reshape(40,80)
    xidata = uf.xi2fullxi(xi.T)
    ximodel = xg.calculate_xi_param(param[0], param[1], param[2], param[3], param[4])
    Q = np.mean(abs(np.log10(1+xidata)-np.log10(1+ximodel)))
    return ximodel, Q

def flattenmodel(ximodel):
    xihalf = (ximodel.T)[40:,:]
    #xiflip = np.flipud(xihalf)
    xiflat = xihalf.flatten()
    return xiflat

def wpxi024_model(ximodel):
    
    rperarr = np.linspace(0.5, 39.5, 40)
    rpararr = np.linspace(-39.5,39.5,80)
    wp2d = flattenmodel(ximodel)
        
    njn=0
    rrarr=np.linspace(1,30,25)
    thetaarr=np.linspace(0,np.pi,101)
    muarr=np.cos(thetaarr)

    #define the mapping between rp_rpi and r mu
    Irmu_dic=GFHOD.rmuweight_mapping(rperarr,rpararr,rrarr,muarr,mode='lin')
    
    wp_xy=wp2d.reshape(rperarr.size,rpararr.size)
    #get the projected correlation function by integrating along los
    wp1d=np.sum(wp_xy, axis=1)
    wp = np.column_stack((rperarr, wp1d))

    #compute xi in s mu space
    xismu=GFHOD.Interpolate_xi(wp_xy,Irmu_dic)

    #get the legendre polynomial from interpolated xismu
    xi024=GFHOD.Xi_Legendre('jnk',rrarr, thetaarr, xismu,samp='rtheta',write=0)
    #xi024[:,1:]=2.0*xi024[:,1:]
    return wp, xi024

def chisq_xi024(xi024model):
    data=np.loadtxt('/disk2/qhang/CovarianceCompare/Data/WPXI024/Galauto_data-rp-pi-NJN-54.txt')
    #cov024_data = np.cov(data[40:,4:])*(54.-1.)
    #corr024_data = uf.corr_mat(cov024_data)
    xi024_data = data[40:,1]
    xi024_model = np.concatenate((xi024model[:,1],xi024model[:,2],xi024model[:,3]))
    
    #eigval, eigvec = cov_eig(cov024_data)
    #new_basis = np.zeros((len(xi024_data),2))
    diff = xi024_data-xi024_model
    #chisq = np.zeros((len(xi024_data),2))
    """
    for ii in range(len(new_basis)):
        new_basis[ii,0] = np.dot(diff,eigvec024_LC[:,ii])
        new_basis[ii,1] = np.dot(diff,eigvec024_v06[:,ii])
    for ii in range(len(new_basis)):  
        chisq[ii,0] = sum(new_basis[:(ii+1),0]**2*eigval024_LC[:(ii+1)]**(-1))
        chisq[ii,1] = sum(new_basis[:(ii+1),1]**2*eigval024_v06[:(ii+1)]**(-1))
    """
    invLC = np.loadtxt('/disk1/qhang/CorrFuncShadab/CMBgal_z0103Code/XI024InvCov_rp40.txt')
    p = np.matmul(invLC,diff)
    chisq = sum(diff*p)    
    return chisq