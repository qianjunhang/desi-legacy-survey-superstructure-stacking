import os
import PairCountTOxi as pcxi

paircount=1
analysis=0

#os.system('cd /disk1/qhang/Projects/CorrFunc_Shadab/')

comm='python /disk1/qhang/Projects/CorrFunc_Shadab/Runme_Correlation.py -sampmode 0 '
comm=comm+'-data /disk2/qhang/ISW_CMB/box/Voids-centre-gal-Rmin0-Rmax500-bins40-tol0.txt '
comm=comm+'-rand /disk2/qhang/ISW_CMB/box/Voids-centre-gal-Rmin0-Rmax500-bins40-tol0.txt '
comm=comm+'-data2 /disk2/qhang/ISW_CMB/box/z0.5_zRSD-HOD_GR_B1024_Box1_ex-s5-g5-gdd-wP-gal.txt '
comm=comm+'-rand2 /disk2/qhang/ISW_CMB/box/z0.5_zRSD-HOD_GR_B1024_Box1_ex-s5-g5-gdd-wP-gal.txt '
comm=comm+'-nbins 20 20 '
comm=comm+'-samplim 0 60 0 1 '
comm=comm+'-pbc 1 '
comm=comm+'-los 1 '
comm=comm+'-njn 0 '
comm=comm+'-outfile /disk2/qhang/ISW_CMB/box/PairCounts/CrossCorr-Voids-centre-gal-Rmin0-Rmax500-bins40-tol0 '
comm=comm+'-filetype txt '
#comm=comm+'-z1z2 0.6 1.1 '
comm=comm+'-randfactor 1 '
comm=comm+'-nproc 8'

if paircount==1:
    print('Running: '+comm)
    os.system(comm)
if analysis==1:
    inroots=[]
    NJNs=[]
    root='/disk2/qhang/ISW_CMB/eBOSS_data/PairCounts/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0'
    njn=0
    xitype='crossAll'
    inroots.append(root)
    NJNs.append(njn)
    samp='rp-pi'
    xi2droot='/disk2/qhang/ISW_CMB/eBOSS_data/XI2D/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0'
    xi02root='/disk2/qhang/ISW_CMB/eBOSS_data/WP/CrossCorr-Voids-centre-vol-Rmin0-Rmax300-bins40-tol0'
    print(inroots,NJNs)
    pcxi.xi2d_wp_xi02_Manyfroot_jn(inroots=inroots,NJNs=NJNs,xi2droot=xi2droot,outroot=xi02root,xitype=xitype,nscomb=1,samp=samp)

