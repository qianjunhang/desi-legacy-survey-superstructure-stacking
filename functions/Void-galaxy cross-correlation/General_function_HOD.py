#This hold many function used in the HOD fitter
from __future__ import print_function

import numpy as np
import collections
import os.path as path

#Shada correlation function package
try:
   import f as ShadabCorr
except:
   print('**************** Warning ********************')
   print('Shadab Correlation function package not found.')
   print('xistat=Shadab is not a usable withour this package')

#Manodeep Correlation function package
try: 
   import Corrfunc
   from Corrfunc.theory import wp as MdeepWP
   from Corrfunc.theory import DDrppi as MdeepDD
except:
   print('**************** Warning ********************')
   print('Manodeep Correlation function package not found.')
   print('xistat=Mdeep is not a usable withour this package')

#Try to imports pandas if available
try:
   import pandas as pd
   pd_flag=1
except:
   pd_flag=0
   print('''pandas not found: you can install pandas from
            https://pypi.python.org/pypi/pandas/0.18.0/#downloads
            The code will work without pandas but pandas will speed up loading large text files
            ''')

#This uses pandas if available otherwise numpy loadtxt to read files
def load_txt(fname):
   if(pd_flag==1):
      tb=pd.read_table(fname,delim_whitespace=True,comment='#',header=None,
              dtype=np.float)
      tb=tb.as_matrix()
   else:
      tb=np.loadtxt(fname)
   return tb


def load_halos(SHOD):
    #This function loads the halos catalogs from different halod types
    #It stores the following essential information of the halos
    #Halos positions, mass and velocities with velocity dispersion
    #it should also store some randomly selected particels if available.

    #everything will held in a ordered dictionary
    SHOD['halos']=collections.OrderedDict()

    if(SHOD['halo_filetype']=='RunPBtxt'):
        htmp=load_txt(SHOD['halo_cat'])
    
        #apply any selections if needed
        ind=htmp[:,3]>0 #only positive non-zero halo mass

        #now assign important propwerites
        SHOD['halos']['xyz']  = htmp[ind,:3]*SHOD['Lbox']
        SHOD['halos']['xyztype']='nonunit' #to say whether halo co-ordinates are in unit box or not
        SHOD['halos']['vxyz'] = htmp[ind,4:7]
        SHOD['halos']['Mvir']= htmp[ind,3]
        SHOD['halos']['Mhalo']= htmp[ind,3]
        SHOD['halos']['sigv'] = htmp[ind,7]

    if(SHOD['halo_filetype']=='bolshoitxt'):
        #first load the halo catalog
        htmp=load_txt(SHOD['halo_cat'])

        #apply any selections if needed
        ind=htmp[:,10]>0 #only positive non-zero halo mass

        #now assign important propwerites
        SHOD['halos']['xyz']  = htmp[ind,17:20]
        SHOD['halos']['xyztype']='nonunit' #to say whether halo co-ordinates are in unit box or not
        SHOD['halos']['vxyz'] = htmp[ind,20:23]
        SHOD['halos']['Mvir']= htmp[ind,10]
        SHOD['halos']['Mhalo']= SHOD['halos']['Mvir']
        SHOD['halos']['sigv'] = htmp[ind,13] 
        SHOD['halos']['r200'] = htmp[ind,11]
        rs=htmp[ind,12]
        SHOD['halos']['rsr200'] = rs/SHOD['halos']['r200']

    if(SHOD['halo_filetype']=='AbacusRockstar'):
        #initilize the dictionaries with empty values
        for tkey in ['xyz','vxyz','Mvir','sigv','r200','rs','rsr200']:
            SHOD['halos'][tkey]=np.array([])
        Kmap={'xyz':'pos','vxyz':'vel','Mvir':'m_SO', 'sigv':'vrms','r200':'r','rs':'rs'}
      
        import h5py
        print('*** Loading halos from directory:',SHOD['halo_dir'])
        fnum=0
        hfname='halos_0.%d.h5'%fnum
        print('Warning: not entirely sure about r200 need to check further')
        SHOD['halos']['xyztype']='nonunit' #to say whether halo co-ordinates are in unit box or not
        while(path.isfile(SHOD['halo_dir']+hfname)):
            print(fnum,hfname)
            htmp=h5py.File(SHOD['halo_dir']+hfname)
            for tkey in Kmap.keys():
               SHOD['halos'][tkey]  = accumulate_arrays(SHOD['halos'][tkey],htmp['halos'][Kmap[tkey]])
            htmp.close()
            fnum=fnum+1
            hfname='halos_0.%d.h5'%fnum

        SHOD['halos']['Mhalo']= SHOD['halos']['Mvir']
        SHOD['halos']['rsr200'] = SHOD['halos']['rs']/SHOD['halos']['r200']

    if(SHOD['halo_filetype']=='Joachimtxt'):
        htmp=load_txt(SHOD['halo_cat'])
    
        #apply any selections if needed
        ind=htmp[:,17]>0 #only positive non-zero halo mass

        #now assign important propwerites
        SHOD['halos']['xyz']  = htmp[ind,:3]*(505.0/3072.0)
        SHOD['halos']['xyztype']='nonunit' #to say whether halo co-ordinates are in unit box or not
        SHOD['halos']['Mvir'] = htmp[ind,17]*(2.8792E+9 / 8.0)
        SHOD['halos']['Mhalo'] = htmp[ind,17]*(2.8792E+9 / 8.0)

    print('******')
    print('Finished loading halos: nhalos=',SHOD['halos']['xyz'].shape[0])

    #halo mass function
    #mhalo_lims=np.linspace(SHOD['halos']['Mhalo'].min(),SHOD['halos']['Mhalo'].min(),100)
    Nhalo, binhalo = np.histogram(np.log10(SHOD['halos']['Mhalo']),bins=100)
    mhalo_mid=np.power(10,0.5*(binhalo[:-1]+binhalo[1:]))
    SHOD['halos']['halohist']=np.column_stack([mhalo_mid,Nhalo])
    #print('halo mass :', mhalo_mid)

    for props in ['xyz','vxyz']:
       if(props  not in SHOD['halos'].keys()):
          continue

       if('v' in props):
           ccpre='v'
       else:
           ccpre=''

       limstr='halo limits :'
       for ii,cc in enumerate(['x','y','z']):
          limstr='%s %s= %12.5e %12.5e '%(limstr,ccpre+cc,SHOD['halos'][props][:,ii].min(),
                SHOD['halos'][props][:,ii].max())
       print(limstr)
    print('******')

    return SHOD

def accumulate_arrays(arr1,arrin):
    if(arr1.size==0):
        arr1=np.copy(arrin)
    elif(len(arrin.shape)==1):
        arr1=np.concatenate([arr1,arrin])
    elif(len(arrin.shape)==2):
        arr1=np.row_stack([arr1,arrin])

    return arr1

def load_stats_data(SHOD):
    #This load the list of statistics needed with its covariance matrix
    #Store the measurement and inverse covariance in the SHOD['data']
    
    SHOD['data']={}
    if(SHOD['stats']=='wp'): #load wp
        wpxiall=np.loadtxt(SHOD['wp_file'])
        wpnjn=wpxiall.shape[1]-4
        #select the scale 
        ind1=wpxiall[:,0]>=SHOD['wp_rp_fit'][0]
        ind2=wpxiall[:,0]<=SHOD['wp_rp_fit'][1]
        indsel_stats=ind1*ind2
        #wpxiall=wpxiall[indsel_stats,:]

        SHOD['data']['rr']=wpsall[indsel_stats,0]
        SHOD['data']['mu']=wpsall[indsel_stats,1]
        SHOD['data']['njn']=wpnjn

    elif(SHOD['stats']=='wpxi024'):
        wpxiall=np.loadtxt(SHOD['wpxis024_file'])
        #read the lines in the file to get njn,nsxi and nwp
        flines=open(SHOD['wpxis024_file'],'r').readlines()
        got=0
        for tline in flines:
            if('#njn=' in tline):
                njn=np.int(tline.split()[1]); got=got+1
            elif('#nwp=' in tline):
                nwp=np.int(tline.split()[1]); got=got+1
            elif('#nsxi=' in tline):
                nsxi=np.int(tline.split()[1]);got=got+1

            if(got>=3):
                break

        #select the scales for wp 
        ind1=wpxiall[:nwp,0]>=SHOD['wp_rp_fit'][0]
        ind2=wpxiall[:nwp,0]<=SHOD['wp_rp_fit'][1]
        indwp=ind1*ind2
        #select the scales for xis024
        ind1=wpxiall[nwp:,0]>=SHOD['xi02_fit'][0]
        ind2=wpxiall[nwp:,0]<=SHOD['xi02_fit'][1]
        indxis024=ind1*ind2

        indsel_stats=np.concatenate((indwp,indxis024))

        #wpxiall=wpxiall[indsel,:]
        wpxinjn=wpxiall.shape[1]-4
        assert(wpxinjn==njn)
        #wpxicov=np.cov(wpxiall[:,4:])*(wpxinjn-1)
        #wpxi_invcov=np.linalg.inv(wpxicov)

        SHOD['data']['rr']=wpxiall[indsel_stats,0]
        SHOD['data']['mu']=wpxiall[indsel_stats,1]
        SHOD['data']['njn']=wpxinjn

        #SHOD['data']['icov']=wpxi_invcov

    #covariance estimation either from jacknife or from file
    if('covariance_file' not in SHOD.keys()):
        wpxicov=np.cov(wpxiall[indsel_stats,4:])*(SHOD['data']['njn']-1)
    else:
        wpxicov_all=np.loadtxt(SHOD['covariance_file'])
        assert(wpxiall.shape[0]==wpxicov_all.shape[0])
        indsel2d=np.zeros(wpxicov_all.size,dtype=bool).reshape(wpxicov_all.shape)
        for tii in range(0,indsel_stats.size):
           for tjj in range(0,indsel_stats.size):
              indsel2d[tii,tjj]=indsel_stats[tii]*indsel_stats[tjj]

        nsel=np.sum(indsel_stats)
        print(np.sum(indsel2d),nsel)
        wpxicov=wpxicov_all[indsel2d].reshape(nsel,nsel)
        print('using covariance from file:',SHOD['covariance_file'], wpxicov.shape)

    wpxi_icov=np.linalg.inv(wpxicov)
    SHOD['data']['icov']=wpxi_icov

    if(0):
        import pylab as pl
        plotcov_corr(wpxicov)
        plotcov_corr(wpxi_icov)
        pl.show()

    return SHOD

def plotcov_corr(covin):
    import pylab as pl
    corr=np.copy(covin)
    for ii in range(0,covin.shape[0]):
        for jj in range(0,covin.shape[1]):
            corr[ii,jj]=covin[ii,jj]/np.sqrt(covin[ii,ii]*covin[jj,jj])

    pl.figure()
    pl.pcolormesh(corr,cmap='seismic',vmin=-1,vmax=1)
    pl.colorbar()


def get_HOD_xistat(SHOD,LD):
    #This function uses the HOD populated galaxy catalog to compute 
    #DD count and convert it to appropriate xi statistics 

    #add the weight column if needed
    if(LD['gcat'].shape[1]<=3):
       LD['gcat']=np.column_stack([LD['gcat'],np.ones(LD['gcat'].shape[0])])
    #else:
    #   LD['gcat'][:,3]=1.0

    #np.savetxt('tmpgcat.txt',SHOD['gcat'][:SHOD['Ngal'],:])

    ## make the data file arra
    data_c=np.ascontiguousarray(LD['gcat'][:LD['Ngal'],[0,1,2,3]], dtype=np.float64)
    #set last column to one to be used as weight
    data_c[:,3]=1.0
    
    if('xisetup' not in SHOD.keys()):
        #These setting assume periodic box z-los
        SHOD['xisetup']={'njn':0,'pbc':1,'los':1,
                'interactive':-1,'nhocells':100}
        SHOD['xisetup']['posmin_c']=np.ascontiguousarray(np.zeros(3))
        SHOD['xisetup']['blen_c']=np.ascontiguousarray(np.zeros(3)+SHOD['Lbox'])
        if 'wp' in SHOD['stats']:
            SHOD['xisetup']['sampmode']=3
            SHOD['wp_rpi']
            rlim=np.array([SHOD['wp_rper'][0],SHOD['wp_rper'][1],
                        SHOD['wp_rpi'][0],SHOD['wp_rpi'][1]],dtype='double')
            SHOD['xisetup']['rlim_c'] =np.ascontiguousarray(rlim)
            SHOD['xisetup']['nbin']=np.array([SHOD['wp_rper'][2],
                SHOD['wp_rpi'][2]],dtype='int')

        #To get the analytical RR pair count for periodic box
        SHOD['rpers']=np.linspace(rlim[0],rlim[1],SHOD['xisetup']['nbin'][0]+1)
        SHOD['rpars']=np.linspace(rlim[2],rlim[3],SHOD['xisetup']['nbin'][1]+1)
        SHOD['rpers_mid']=np.power(10,0.5*(SHOD['rpers'][1:]+SHOD['rpers'][:-1]))
        n2dbin=SHOD['xisetup']['nbin'][0]*SHOD['xisetup']['nbin'][1]
        SHOD['RRvol']=np.zeros(n2dbin).reshape(
                     SHOD['xisetup']['nbin'][0],SHOD['xisetup']['nbin'][1])
        SHOD['dr_par']=np.mean(SHOD['rpars'][1:]-SHOD['rpars'][:-1])
        for pp in range(0,SHOD['xisetup']['nbin'][0]):
           for qq in range(0,SHOD['xisetup']['nbin'][1]):
              r1=np.power(10,SHOD['rpers'][pp]);
              r2=np.power(10,SHOD['rpers'][pp+1])
              Area_el=np.pi*(r2*r2-r1*r1)
              SHOD['RRvol'][pp,qq]=(SHOD['rpars'][qq+1]-
                        SHOD['rpars'][qq])*Area_el/np.power(SHOD['Lbox'],3)
              
    if('nhocells' in SHOD.keys()):
        SHOD['xisetup']['nhocells']=SHOD['nhocells']
        print('Using nhocells:',SHOD['nhocells'])

    #To get the DD pair count
    LD['pcDD']= ShadabCorr.corr2dpy(data_c, data_c, SHOD['xisetup']['rlim_c'], 
            SHOD['xisetup']['nbin'][0] ,SHOD['xisetup']['nbin'][1], 
            SHOD['xisetup']['nhocells'],SHOD['xisetup']['blen_c'],
            SHOD['xisetup']['posmin_c'],SHOD['xisetup']['sampmode'],
            SHOD['xisetup']['njn'],
            SHOD['xisetup']['pbc'],SHOD['xisetup']['los'],
            SHOD['xisetup']['interactive'])

    RR=SHOD['RRvol']*np.power(LD['Ngal'],2)
    wprppi=(1.0*LD['pcDD']/RR)-1.0
    wprp=np.sum(wprppi,axis=1)*SHOD['dr_par']
    LD['model']=np.column_stack([SHOD['rpers_mid'],wprp])
   

    if(0): #debug
        import pylab as pl
        pl.plot(wprppi.reshape(wprppi.size),'.')
        pl.figure()
        pl.plot(SHOD['model'][:,0],SHOD['model'][:,1])
        pl.xscale('log')
        pl.show()
    return SHOD, LD


def Analytic_volume(rpers,pimax,dpi=1,ngal=1,boxsize=1):
    #estimate the volume or RR count analytically for periodic box
    rmax=rpers[1:]; rmin=rpers[:-1]
    RR_x=np.pi*(np.power(rmax,2)-np.power(rmin,2))*dpi*np.power(ngal,2)/np.power(boxsize,3)
    npi=np.int(pimax)
    RR_xy=np.zeros(RR_x.size*npi).reshape(RR_x.size,npi)
    for ii in range(0,npi):
       RR_xy[:,ii]=RR_x
    return RR_xy

##function to get r,mu interpolation weights
def rmuweight_mapping(rperarr,rpararr,rrarr,muarr,mode='lin'):
    #mode: whether to interpolate in lin or log space: mode= 'lin', 'log'
    #WARNING: mode = 'log' doesn't work very well now, need to figure out issue, only use lin
    #inputs are arrays of samplings
    #get the sizes
    nr=rrarr.size; nmu=muarr.size
    
    #a dictionary to store all the output
    Irmu_dic={}
    
    #Wmat store the mapping from rpar,rpi to r,mu
    #Wmat[nr x nmu x 4 nearest point x 3(ix,iy,w)]
    Wmat=np.zeros(nr*nmu*4).reshape(nr,nmu,4)
    Wind=np.zeros(nr*nmu*4*2,dtype=int).reshape(nr,nmu,4,2)
    rmumat=np.zeros(nr*nmu*4).reshape(nr,nmu,4)
    
    #sintheta
    stheta=np.sqrt(1-(muarr*muarr))
    
    indx=[-1,-1]; indy=[-1,-1]
    ccmat=np.zeros(16).reshape(4,4)
    for ir in range(0,nr):
        for imu in range(0,nmu):
            yy=rrarr[ir]*muarr[imu]
            xx=rrarr[ir]*stheta[imu]
            
            #save the rmu list including its cartesian version
            rmumat[ir,imu,:]=np.array([xx,yy,rrarr[ir],muarr[imu]])
            
            #get the indices for x-axis (rper)
            indx[0]=np.argmin(np.abs(xx-rperarr))
            if(xx<=rperarr[0]):
                indx[0]=0;indx[1]=1
            elif(xx>=rperarr[-1]):
                indx[1]=rperarr.size-1; indx[0]=indx[1]-1
            elif(rperarr[indx[0]]<xx):
                indx[1]=indx[0]+1
            else:
                indx[1]=indx[0]; indx[0]=indx[1]-1
            
            #get the indices for y-axis (rpar)
            indy[0]=np.argmin(np.abs(yy-rpararr))
            if(yy<=rpararr[0]):
                indy[0]=0;indy[1]=1
            elif(yy>=rpararr[-1]):
                indy[1]=rpararr.size-1; indy[0]=indy[1]-1
            elif(rpararr[indy[0]]<yy):
                indy[1]=indy[0]+1
            else:
                indy[1]=indy[0]; indy[0]=indy[1]-1
                
            #now fill wmat with the indices and weights
            for ii in range(0,2):
                for jj in range(0,2):
                    Wind[ir,imu,ii*2+jj,0]=np.int(indx[ii]) #x-indices
                    Wind[ir,imu,ii*2+jj,1]=np.int(indy[jj]) #y-indices
                    if(mode=='lin'):
                        #bilinear solution
                        ccmat[ii*2+jj,:]=np.array([1,rperarr[indx[ii]],rpararr[indy[jj]],
                                               rperarr[indx[ii]]*rpararr[indy[jj]]])
                    elif(mode=='log'):
                        #bilinear solution
                        ccmat[ii*2+jj,:]=np.array([1,np.log10(rperarr[indx[ii]]),np.log10(rpararr[indy[jj]]),
                                               np.log10(rperarr[indx[ii]])*np.log10(rpararr[indy[jj]])])
            
            #get the coefficient b11,b12,b21,b22 https://en.wikipedia.org/wiki/Bilinear_interpolation
            #solution will be f(x,y)= b11 f(x1,y1)+ b12 f(x1,y2)+b21 f(x2,y1)+b22 f(x2,y2)
            if(mode=='lin'):
                thisvector=np.array([1,xx,yy,xx*yy])
            elif(mode=='log'):
                thisvector=np.array([1,np.log10(xx),np.log10(yy),np.log10(xx)*np.log10(yy)])
            ccinv=np.linalg.inv(ccmat)
            bb_res=np.dot(ccinv.T,thisvector)
            Wmat[ir,imu,:]=np.copy(bb_res)
            
    Irmu_dic['Wmat']=Wmat
    Irmu_dic['Wind']=Wind
    Irmu_dic['rmumat']=rmumat
    Irmu_dic['mode']=mode
    return Irmu_dic

def Interpolate_xi(wp_xy,Irmu_dic):
    #interpolate the correlation function bilinearly using maps provides rp_pi to rmu
    nr =Irmu_dic['rmumat'].shape[0]
    nmu=Irmu_dic['rmumat'].shape[1]
    
    xismu=np.zeros(nr*nmu).reshape(nr,nmu)
        
    #write a matrix based interpolation
    if(Irmu_dic['mode']=='log'):
        wp_xy=np.log10(wp_xy)
        
    for ii in range(0,4):
        xismu=xismu+Irmu_dic['Wmat'][:,:,ii]*wp_xy[Irmu_dic['Wind'][:,:,ii,0],Irmu_dic['Wind'][:,:,ii,1]]
        
    if(Irmu_dic['mode']=='log'):
        xismu=np.power(10,xismu)
    
    return xismu

def read_wp2d(fname):
    #read the projected correlation function from my code output format
    wp2d=np.loadtxt(fname)
    
    flines=open(fname).readlines()
    
    #read the rper
    ii=-1;got=0
    while(got<2):
        ii=ii+1
        if('#rper:' in flines[ii]):
            rpstr=flines[ii][6:].split()
            rperarr=np.array([])
            for tt in rpstr:
                rperarr=np.append(rperarr,np.float(tt))
            got=got+1
        elif('#rpar:' in flines[ii]):
            rpstr=flines[ii][6:].split()
            rpararr=np.array([])
            for tt in rpstr:
                rpararr=np.append(rpararr,np.float(tt))
            got=got+1
                
    return rperarr,rpararr, wp2d

###To etimate the multipole moment
def Xi_Legendre(root,srebins, murebins, xi2drebin,samp='rmu',write=1):
   if(samp=='rmu'):
      dmu=np.average(murebins[1:]-murebins[:-1])
      diffmu=1 #to account for the fact that integral is 0 to 1
   elif(samp=='rtheta' or samp=='logr-theta'): #in this case murebin is actually theta and not mu
      dtheta=np.average(murebins[1:]-murebins[:-1])
      dmu=np.sin(murebins)*dtheta
      murebins=np.cos(murebins) #this converts theta to mu for integration
      diffmu=2 #to account for the fact that itegral is -1 to 1

   mu2=np.power(murebins,2)
   P0 =np.ones(mu2.size)/2.0    #(2l+1)Pl(mu)*sqrt(1-mu^2) monopole term
   P2=2.5*(3*mu2-1)/2.0         #p2=(3mu^2-1)/2  quadrupole term
   #P1=3*murebins/2.0
   P4=4.5*(35.0*mu2*mu2-30.0*mu2+3.0)/8.0

   ns=srebins.size 
   xi02=np.zeros(ns*4).reshape(ns,4)

   if(write):
      outfile=root+'-xi02.dat'
      f_write.append(outfile)
      fout=open(outfile,'w')

   for ii in range(0,ns):
      xi02[ii,0]=srebins[ii]
      xi02[ii,1]=np.sum(xi2drebin[ii,:]*P0*dmu)*2.0/diffmu
      xi02[ii,2]=np.sum(xi2drebin[ii,:]*P2*dmu)*2.0/diffmu
      xi02[ii,3]=np.sum(xi2drebin[ii,:]*P4*dmu)*2.0/diffmu
      if(write==1 and samp=='rmu'):
         fout.write('%12.8lf %12.8lf %12.8lf\n' %(xi02[ii,0],xi02[ii,1],xi02[ii,2]))
      if(write==1 and samp=='rtheta'):
         fout.write('%12.8lf %12.8lf %12.8lf %12.8lf\n' %(xi02[ii,0],xi02[ii,1],xi02[ii,2],xi02[ii,3]))

   
   #plot_xi02(xi02)
   return xi02


def get_HOD_xistatdeep(SHOD,LD):
    #np.savetxt('tmpgcat.txt',SHOD['gcat'][:SHOD['Ngal'],:])
    nthreads=4
    if('xisetup' not in SHOD.keys()):
        rlim=np.array([SHOD['wp_rper'][0],SHOD['wp_rper'][1],
                        SHOD['wp_rpi'][0],SHOD['wp_rpi'][1]],dtype='double')
        SHOD['rpers']=np.linspace(rlim[0],rlim[1],SHOD['wp_rper'][2]+1)
        #SHOD['rpers_mid']=np.power(10,0.5*(SHOD['rpers'][1:]+SHOD['rpers'][:-1]))
        SHOD['rpers']=np.power(10,SHOD['rpers']) #take antilog needed for Manodeep xi code
        SHOD['rpers_mid']=0.5*(SHOD['rpers'][1:]+SHOD['rpers'][:-1])
        SHOD['rpars']=np.linspace(rlim[2],rlim[3],np.int(2*rlim[3]+1))
        SHOD['rpars_mid']=0.5*(SHOD['rpars'][1:]+SHOD['rpars'][:-1])
        SHOD['pimax']=rlim[3]

        #compute the volume factor RR count for correlaton function
        SHOD['RR_xy']=Analytic_volume(SHOD['rpers'],SHOD['pimax'],dpi=2,ngal=1,
                                boxsize=SHOD['Lbox'])

        if('xi' in SHOD['stats']):
            #r sampling for multipoles calculation
            SHOD['xirr']=np.power(10,np.linspace(SHOD['xi02_logr'][0],
                SHOD['xi02_logr'][1],SHOD['xi02_logr'][2]))
            #theta sampling for multipole calculation
            SHOD['thetaarr']=np.linspace(0,np.pi,101)
            SHOD['muarr']=np.cos(SHOD['thetaarr'])
            #Define the mapping ofr rp_rpi to rmu for interpolation
            SHOD['Irmu_dic']=rmuweight_mapping(SHOD['rpers_mid'],SHOD['rpars_mid'],
                    SHOD['xirr'],SHOD['muarr'],mode='lin')

    #first get the paircount
    DD_result= MdeepDD(1,SHOD['nthreads'], SHOD['pimax'], SHOD['rpers'], 
       np.float32(LD['gcat'][:LD['Ngal'],0]), np.float32(LD['gcat'][:LD['Ngal'],1]),
       np.float32(LD['gcat'][:LD['Ngal'],2]), weights1=None, periodic=True,
       X2=None, Y2=None, Z2=None, weights2=None, verbose=False, boxsize=SHOD['Lbox'],
       output_rpavg=False, xbin_refine_factor=2, ybin_refine_factor=2,
       zbin_refine_factor=1, max_cells_per_dim=100,
       c_api_timer=False, isa=r'fastest', weight_type=None)    
    #estimate wp_xy
    wp_xy=DD_result['npairs'].reshape(SHOD['rpers_mid'].size,np.int(SHOD['pimax']))
    wp_xy=(wp_xy/(np.power(LD['Ngal'],2)*SHOD['RR_xy']))-1.0
    #projected correlation function
    wp1d=2.0*np.sum(wp_xy,axis=1)
    LD['model']=np.column_stack([SHOD['rpers_mid'],wp1d])

    if('xi' in SHOD['stats']):
       #get wp_xy including negative rpi
       wp_xyAll=np.column_stack([wp_xy[:,::-1],wp_xy])
       xismu=Interpolate_xi(wp_xyAll,SHOD['Irmu_dic'])
       xi02=Xi_Legendre('jnk',SHOD['xirr'], SHOD['thetaarr'], xismu,
              samp='rtheta',write=0)
       LD['model']=np.row_stack([LD['model'],xi02[:,[0,1]]])
       LD['model']=np.row_stack([LD['model'],xi02[:,[0,2]]])
       if('xi024' in SHOD['stats']):
          LD['model']=np.row_stack([LD['model'],xi02[:,[0,3]]])

    if('xilike' not in SHOD.keys()):
       SHOD['xilike']={}
       if('wp' in SHOD['stats']):
          ind1=SHOD['rpers_mid']>=SHOD['wp_rp_fit'][0]
          ind2=SHOD['rpers_mid']<=SHOD['wp_rp_fit'][1]
          SHOD['xilike']['indsel']=ind1*ind2
       if('xi' in SHOD['stats']):
          ind1=xi02[:,0]>=SHOD['xi02_fit'][0]
          ind2=xi02[:,0]<=SHOD['xi02_fit'][1]
          ind=ind1*ind2
          #for monopole and quadrupole
          SHOD['xilike']['indsel']=np.concatenate((SHOD['xilike']['indsel'],ind,ind))
          if('xi024' in SHOD['stats']): #for hexadecapole
             SHOD['xilike']['indsel']=np.concatenate((SHOD['xilike']['indsel'],ind))


    if(0):
       wp_results = MdeepWP(SHOD['Lbox'], SHOD['wp_rp_fit'][1], nthreads, 
             SHOD['rpers'], 
            LD['gcat'][:LD['Ngal'],0],LD['gcat'][:LD['Ngal'],1], 
            LD['gcat'][:LD['Ngal'],2], verbose=False, output_rpavg=False)

       LD['model']=np.column_stack([SHOD['rpers_mid'],wp_results['wp']])

    return SHOD, LD

