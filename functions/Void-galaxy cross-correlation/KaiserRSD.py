#Author: Shadab Alam, June 2016
#This compute the redshift space correlation function

import numpy as np
import scipy.integrate as integrate
import scipy.special as special
from scipy.interpolate import interp1d
from scipy import interpolate
import scipy.integrate as integrate

import pylab as pl


def LogScaleCubicInterp(Pw_raw):
    #copied from Siddharth
    # Linear interpolation is being at the log scale because of the original file (raw power spectrum) is evenly spaced at the log scale
    Pw_log = np.log(Pw_raw)
    count = 0
    xstore = Pw_log[:,0]
    ystore = Pw_log[:,1]
    while count < 1:
        x_cubic_interp = np.linspace( xstore[0] , xstore[len(xstore)-1] , num=250*(len(xstore)-1) , endpoint=True )
        f2 = interp1d(xstore, ystore, kind='cubic')
        y_cubic_interp = f2( x_cubic_interp )

        xstore = x_cubic_interp
        ystore = y_cubic_interp

        count = count + 1

    xstore = xstore.reshape( len(xstore), 1)
    ystore = ystore.reshape( len(ystore), 1)
    Pw = np.concatenate( (np.exp(xstore), np.exp(ystore)), axis=1 )
    #np.savetxt('Pw_interp.dat', Pw, delimiter='    ')
    return Pw

def LegendrePK(plink,beta=0):
   kk=plink[:,0]
   #From Hamilton 1997 review (equation 5.7) arxiv: 9708102
   P0k=(1.0+(2.0*beta/3.0)+(1.0*beta*beta/5.0))*plink[:,1]
   P2k=((4.0*beta/3.0)+(4.0*beta*beta/7.0))*plink[:,1]
   P4k=8.0*beta*beta*plink[:,1]/35.0

   pk024=np.column_stack([kk,P0k,P2k,P4k])
   return pk024
   
def interpSphj(zz,jnz,factor=4):
   zind=np.floor(factor*zz).astype('int')
   assert(np.max(zind)<jnz.shape[0]-1),"print hello %d %d"%(np.max(zind),jnz.shape[0])
   sph024=np.zeros(zz.size*3).reshape(zz.size,3)

   for ii,ell in enumerate([0,2,4]):
      slopes=(jnz[zind+1,ell+1]-jnz[zind,ell+1])/(jnz[zind+1,0]-jnz[zind,0])
      intercep=jnz[zind,ell+1]-slopes*jnz[zind,0]
      sph024[:,ii]=slopes*zz+intercep

   return sph024

def InterpSpBessel(zmin=0,zmax=10,nz=100):
   zz=np.linspace(zmin,zmax,nz)
   jnz=np.zeros(nz*5).reshape(nz,5)
   for ii in range(0,nz):
     jnz[ii,:]=special.sph_jn(4,zz[ii])[0]

   #Note the spherical bessel function for some reason becomes nan for large z 
   #but since its close to zero we are setting it to zero as an approximation
   #This should effect only very large scale and should have small effect
   ind=jnz[:,0]!=jnz[:,0]
   jnz[ind,:]=0
   jnz=np.column_stack([zz,jnz])

   return jnz

def LegendreXis(pk024,smin=0,smax=200,ns=100):
   ss=np.linspace(smin,smax,ns)
   xis024=np.zeros(ns*4).reshape(ns,4)
   xis024[:,0]=ss

   # Deciding ranges for the momentum space
   ks= 0; kc = pk024[:,0].size-1
   dk=pk024[ks+1:kc+1:,0]-pk024[ks:kc,0]
   
   #evaluate the spherical bessel function
   zmin=0; zmax=np.ceil(ss[-1]*pk024[-1,0])+5
   factor=4  #decides the sampling of bessel function for intepolation 1/factor
   jnz=InterpSpBessel(zmin=zmin,zmax=zmax,nz=np.int(factor*zmax+1))
   if(0):
      #interpolate the spherical bessel function
      zz=np.linspace(zmin,zmax-5,10000)
      print(zmin,zmax,jnz[-20:,:])
      sph024=interpSphj(zz,jnz,factor=factor)
      for ii,ell in enumerate([0,2,4]):
         pl.plot(jnz[:,0],jnz[:,ell+1],'rs')
         pl.plot(zz,sph024[:,ii],'b-')
         pl.show()

   #compute the parts independent of r
   pfell=np.zeros((kc-ks)*3).reshape(kc-ks,3)
   for ii,ell in enumerate([0,2,4]):
      pfell[:,ii]=np.power(complex(0,1),ell).real #decide sign
      pfell[:,ii]=pfell[:,ii]*np.power(pk024[ks:kc,0],2)*pk024[ks:kc,ii+1]
      pfell[:,ii]=pfell[:,ii]/(2.0*np.pi*np.pi)

   #for integration with bessel function
   for jj in range(0,ns):
      zz=pk024[ks:kc,0]*ss[jj]
      jellkr=interpSphj(zz,jnz,factor=factor)
      for ii,ell in enumerate([0,2,4]):
         xis024[jj,ii+1]=np.sum(pfell[:,ii]*jellkr[:,ii]*dk)
 
         if(0):
            pl.plot(pk024[ks:kc,0]*ss[jj],jellkr)
            pl.show()
   
   return xis024


def pktoxi(pkin,beta=0,xi2d=0,plots=1,smin=0,smax=152,ns=77):
   '''This function takes the input pk and compute the redshift space
   correlation function using kaiser formula, 
   pkin should be array with two column first column k and second pk 
   in CAMB units'''
   
   #interpolate linearly
   plink=LogScaleCubicInterp(pkin)
   if(0):
      pl.plot(pkin[:,0],pkin[:,1],'r.')
      pl.plot(plink[:,0],plink[:,1],'b.')
      pl.xscale('log')
      pl.yscale('log')
      pl.show()

   #get the legendre moments
   pk024 = LegendrePK(plink,beta=beta)
   if(0):
      import pylab as pl
      pl.plot(pk024[:,0],pk024[:,1],'r.')
      pl.plot(pk024[:,0],pk024[:,2],'b.')
      pl.plot(pk024[:,0],pk024[:,3],'g.')
      pl.xscale('log')
      pl.yscale('log')
      pl.show()

   #get the legendre moments


   #get the legendre moments in xi
   xis024=LegendreXis(pk024,smin=smin,smax=smax,ns=ns)
   if(plots==1):
      print(xis024)
      ss=xis024[:,0]
      import pylab as pl
      pl.plot(ss,np.power(ss,2)*xis024[:,1],label=r'$\xi_0$') 
      pl.plot(ss,np.power(ss,2)*xis024[:,2],label=r'$\xi_2$') 
      pl.plot(ss,np.power(ss,2)*xis024[:,3],label=r'$\xi_4$')
      pl.xlabel(r'$r {\rm (Mpc/h)}$',fontsize=24)
      pl.ylabel(r'$r^2 \xi_\ell$',fontsize=24)
      pl.legend(fontsize=18)
      pl.tight_layout()
      pl.show()

   return xis024

def wp_theory(pkin,rp,rpi_lim=100,nrpi=11,beta=0.4):
   #define some sampling points 
   nrp=rp.size
   rpi_arr=np.linspace(-rpi_lim,rpi_lim,nrpi)
    
   #first get the multipole moments with RSD
   smax=max([np.max(rp),rpi_lim])+10
   xis024=pktoxi(pkin,beta=beta,xi2d=0,plots=0,smin=0,smax=smax,ns=10*rp.size)

   #interpolate the monopole/Quad and hexa
   Ixi0=interpolate.splrep(xis024[:,0],xis024[:,1], s=0,k=1)
   Ixi2=interpolate.splrep(xis024[:,0],xis024[:,2], s=0,k=1)
   Ixi4=interpolate.splrep(xis024[:,0],xis024[:,3], s=0,k=1)
    
    #xi_setup['xired'][ii,mm],err=quad(lambda x: interpolate.splev(x,I_integrand, der=0),rp_arr[1],rp_arr[-2])

   #now create the grid of rp rpi with r,mu for xi2d
   #define the co-ordinate to get xi2d
   ss_2d=np.zeros(nrp*nrpi).reshape(nrp,nrpi)
   mu_2d=np.zeros(nrp*nrpi).reshape(nrp,nrpi)
   xi_2d=np.zeros(nrp*nrpi).reshape(nrp,nrpi)
   for ii in range(0,nrpi):
      ss_2d[:,ii]=np.sqrt(rp*rp+rpi_arr[ii]*rpi_arr[ii])
      mu_2d[:,ii]=rpi_arr[ii]/ss_2d[:,ii]
      #evaluate multipole functions
      P2_mu=0.5*(3.0*mu_2d[:,ii]*mu_2d[:,ii]-1.0)
      P4_mu=0.125*(35.0*np.power(mu_2d[:,ii],4)-30.0*np.power(mu_2d[:,ii],2)+3.0)
        
      #interpolate the multipoles at ss
      xi0_tmp=interpolate.splev(ss_2d[:,ii],Ixi0, der=0)
      xi2_tmp=interpolate.splev(ss_2d[:,ii],Ixi2, der=0)
      xi4_tmp=interpolate.splev(ss_2d[:,ii],Ixi4, der=0)
    
      #now evaluate the xi2d using multipole for ss,mu
      xi_2d[:,ii]=xi0_tmp+xi2_tmp*P2_mu+xi4_tmp*P4_mu
    
   #now convert xi2d to wp
   wp=np.zeros(nrp);
   dr_par=np.mean(rpi_arr[1:]-rpi_arr[:-1])
   for ii in range(0,nrp):
      wp[ii]=np.sum(xi_2d[ii,:])*dr_par
   return np.column_stack([rp,wp])

def M12_r(rr,xir,db,smin=0,smax=100,ns=40,Hz=100,plots=0):
   '''This compute M12 from equation 3 of https://arxiv.org/pdf/1304.4124.pdf'''
   #rhobar=1
   G = 43007.1e-3 #(unit of Mpc/(10^10M_sun)*(km/sec)),
   rhobar = 3* (Hz*Hz)/ (8 * np.pi * G) #in unit of h^2. 
    
   #interpolate the integrand correlation function
   print '4.0*np.pi*rhobar',4.0*np.pi*rhobar
   integrand=4.0*np.pi*rhobar*db*xir*np.power(rr,2)
   f2 = interp1d(rr, integrand, kind='cubic')
   
   ss=np.linspace(smin,smax,ns)
   M12=np.zeros(ns)

   #integrate
   result=integrate.quad(lambda x: f2(x), 0, ss[0])
   M12[0]=result[0]
   for ii in range(1,ns):
      M12[ii]=M12[ii-1]+integrate.quad(lambda x: f2(x), ss[ii-1], ss[ii])[0]

   if(plots==1):
      pl.plot(ss,M12,'ro-')
      pl.xlabel('r')
      pl.ylabel('M12(r)')
      pl.show()

   M12s=np.column_stack([ss,M12])
   return M12s

def zg_r(rr,M12,smin=0,smax=100,ns=40,plots=0):
   '''This compute zg from equation 2 of https://arxiv.org/pdf/1304.4124.pdf'''
   #G=1.0;c=1.0;
   G = 43007.1e-3 #(unit of Mpc/(10^10M_sun)*(km/sec)), 
   c = 299792.458 

   integrand=(G/c)*M12/np.power(rr,2)
   print 'G/c:',G/c
   ind=rr>0
   f2=interp1d(rr[ind],integrand[ind],kind='cubic')


   smin=max([np.min(rr[ind]),smin])
   ss=np.linspace(smin,smax,ns)
   zg=np.zeros(ns)

   #integrate
   result=integrate.quad(lambda x: f2(x), ss[-1], ss[-2])
   zg[-2]=result[0]
   for ii in range(3,ns+1):
      zg[-ii]=zg[-ii+1]+integrate.quad(lambda x: f2(x), ss[-ii+1], ss[-ii])[0]
   
   #intercept at zeros
   slope=(zg[1]-zg[0])/(ss[1]-ss[0])
   intercept=zg[0]-(slope*ss[0])
   ss[0]=0;zg[0]=intercept;
    
   if(plots==1):
      pl.plot(ss,zg,'ro-',lw=2,label='equation 2')
      pl.plot(ss,zg[0]-zg,'b-',lw=2,label='zg(0)-zg(r)')

      pl.xlabel('r',fontsize=24)
      pl.ylabel(r'$z_g$(r)',fontsize=24)
      pl.legend(fontsize=16,loc=4)
      pl.tight_layout()
      pl.show()

   zgs=np.column_stack([ss,zg])
   return zgs


if __name__ == "__main__":
   import argparse
   parser = argparse.ArgumentParser(description='It reads a power spectrum and computes the correlation function for given beta')
   parser.add_argument('-pkfile' ,default='wmap_pk_0.57.dat')
   parser.add_argument('-xifile' ,default='')
   parser.add_argument('-beta' ,type=float,default=0.4)
   parser.add_argument('-smin' ,type=float,default=26.5,help='minimum sbin')
   parser.add_argument('-smax' ,type=float,default=118.5,help='maximum sbin')
   parser.add_argument('-ns' ,type=int,default=24,help='number of s bin')


   args = parser.parse_args()
   print('**********INPUTS**********')
   print('power spectrum file: ',args.pkfile)
   print('beta ',args.beta)
   print('smin:' ,args.smin)
   print('smax:' ,args.smax)
   print('ns  :' ,args.ns)
   print('**************************')

   pkfile=args.pkfile
   if(args.xifile==''):
      xifile=pkfile+'.xi'
   else:
      xifile=args.xifile

   pkin=np.loadtxt(pkfile)
   xis024=pktoxi(pkin,beta=args.beta,xi2d=1,plots=0,smin=args.smin,smax=args.smax,ns=args.ns)
   np.savetxt(xifile,xis024)

   print('Written: ',xifile)
   print('**************')
