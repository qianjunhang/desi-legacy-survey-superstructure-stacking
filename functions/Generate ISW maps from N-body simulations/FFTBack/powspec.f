        module common
        real, allocatable ::pden(:,:,:), pdenvx(:,:,:)
        real, allocatable ::pdenvy(:,:,:), pdenvz(:,:,:)
       real, allocatable :: power(:,:,:), power1(:,:,:)
       real, allocatable :: powervx(:,:,:), powervx1(:,:,:)    
       real, allocatable :: powervy(:,:,:), powervy1(:,:,:)
       real, allocatable :: powervz(:,:,:), powervz1(:,:,:)
      real, allocatable :: powerphi(:,:,:), powerphi1(:,:,:)   
        end module common
        use common

      character*128 infile,outfile
      character*70 outfile_grids, base, base0, file1, file2
      character*70 file3, file4, file5, file6, file7, file8
      character*20 model
      integer iargc
      include 'block.inc'
      include 'ngrid.inc'
      include 'nlevels.inc'
      
      real*8 bin(num_bin), bin_un(num_bin), bin_phi(num_bin)
      real k_min,k_max,binfac,kk,p_es
      real koffset,fact,ak,twopi,poww,poww_phi,shotlimit,conv 
      character*2  no_file
      character*3 no_sim
      character*4  no_z, cnpt
      character*5  cside
      integer nmodes(num_bin),k_bin,isub, isign, isign2
      parameter (twopi=2.*3.1415926)
      parameter (Ho=100.0)
      parameter (om=0.307115)
      real SFac
      real phi, Lfac, Hz, z, omz, Dz, fac2, Hzz
      real kx,ky,kz, nk
      real, allocatable :: datafb(:,:,:)

       isign=1
      isign2=-1      
      call getarg(1,no_sim)
      write(6,*) no_sim
       print*, no_sim
      call read_gadget(no_sim,no_file,npart,aa,side)
       z=redshift
       write(*,*)'z,      Hz,      omz,     Dz,     boxscale'
      write(*,*)z,Hz(z,om), omz(z,om), Dz(z,om), boxscale
       Hzz=Hz(z,om)
      if(z<10.0)then
      write(unit=no_z,fmt='(f4.2)')z
      end if
        
      if(z>=10.0)then
      write(unit=no_z,fmt='(f4.1)')z
      end if

       if(npt<1000)then
       write(unit=cnpt, fmt='(I3)')npt
       endif

       if(npt>=1000)then
       write(unit=cnpt, fmt='(I4)')npt
       endif
                                                                                                                                                                                  
       if(side<1000.0)then
       write(unit=cside, fmt='(f4.0)')side
       endif

       if(side>=1000.0)then
       write(unit=cside, fmt='(f5.0)')side
       endif

         base=trim(trim(no_sim)//'_N'//trim(cnpt)//'_L'//trim(cside))
      outfile='./data/PSphiL1a'//base
      outfile_grids='./dataGrids/Gridsa'//base

      base0=trim('../FFTForward/dataGrids/')

      file1=trim(trim(base0)//'power.'//base)
      file2=trim(trim(base0)//'power1.'//base)
      file3=trim(base0)//'powervx.'//base
      file4=trim(base0)//'powervx1.'//base
      file5=trim(base0)//'powervy.'//base
      file6=trim(base0)//'powervy1.'//base
      file7=trim(base0)//'powervz.'//base
      file8=trim(base0)//'powervz1.'//base
      print*, file1 
      print*, file2 
      print*, file3 
      print*, file4 
      print*, file5 
      print*, file6
      print*, file7
      print*, file8

      open (14,file=outfile,status='unknown')

      do i=1,num_bin
        nmodes(i) = 0
        bin(i)= 0
        bin_un(i)=0
      enddo
         
        k_min=twopi/side
        k_max=twopi*float(npt/2)/side 
        binfac=num_bin/(log(k_max)-log(k_min))
        print*,'side,num_bin,k_min,k_max'
        print*,side,num_bin,k_min,k_max

        allocate(power(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
        allocate(power1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
        allocate(powervx(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
       allocate(powervx1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
        allocate(powervy(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
       allocate(powervy1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
        allocate(powervz(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
       allocate(powervz1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:-npt/4))
       allocate(powerphi(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))
       allocate(powerphi1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))
           power(:,:,:)=0.0
           power1(:,:,:)=0.0
           powervx(:,:,:)=0.0
           powervx1(:,:,:)=0.0
           powervy(:,:,:)=0.0
           powervy1(:,:,:)=0.0
           powervz(:,:,:)=0.0
           powervz1(:,:,:)=0.0
           powerphi(:,:,:)=0.0
           powerphi1(:,:,:)=0.0

         open (1,file=file1,FORM="unformatted")
         open (2,file=file2,FORM="unformatted")
         open (3,file=file3,FORM="unformatted")
         open (4,file=file4,FORM="unformatted")
         open (5,file=file5,FORM="unformatted")
         open (9,file=file6,FORM="unformatted")
         open (7,file=file7,FORM="unformatted")
         open (8,file=file8,FORM="unformatted")

         do ii=1, 4 
         read(1)power
         read(2)power1
         read(3)powervx
         read(4)powervx1
         read(5)powervy
         read(9)powervy1
         read(7)powervz
         read(8)powervz1
 
          do k=-npt/2+1,-npt/4
             kp=k+(ii-1)*npt/4
           do j=-npt/2+1,npt/2
            do i=-npt/2+1,npt/2

          kk=i**2.+j**2.+kp**2.
          r_k=kk**(1./2.)*twopi/side
          k_bin=log(r_k/k_min)*binfac 
          kx=(float(i)*twopi/side)
          ky=(float(j)*twopi/side)
          kz=(float(kp)*twopi/side)
          nk=(float(npt/2)*twopi/side)
          if(r_k .gt. 0.0 .and. r_k .le. k_max) then
     
          k_bin=log(r_k/k_min)*binfac 
          ri = 3.1415926535*real(abs(i))/real(npt)
          rj = 3.1415926535*real(abs(j))/real(npt)
          rk = 3.1415926535*real(abs(kp))/real(npt)
           fac = 1.0
          if (ri.ne.0.) fac = fac * sin(ri)/ri
          if (rj.ne.0.) fac = fac * sin(rj)/rj
          if (rk.ne.0.) fac = fac * sin(rk)/rk 

           fac2=(Hzz**2.0)/(1.0+z)
           powervx(i,j,k)=kx*powervx(i,j,k)/sqrt(fac2)
           powervx1(i,j,k)=kx*powervx1(i,j,k)/sqrt(fac2)
           powervy(i,j,k)=ky*powervy(i,j,k)/sqrt(fac2)
           powervy1(i,j,k)=ky*powervy1(i,j,k)/sqrt(fac2)
           powervz(i,j,k)=kz*powervz(i,j,k)/sqrt(fac2)
           powervz1(i,j,k)=kz*powervz1(i,j,k)/sqrt(fac2)

        powerphi(i,j,kp)= 
     $  (power(i,j,k)+(powervx1(i,j,k)+powervy1(i,j,k)+powervz1(i,j,k)))
     $   /r_k**2
         powerphi1(i,j,kp)=
     $  (power1(i,j,k)-(powervx(i,j,k)+powervy(i,j,k)+powervz(i,j,k)))
     $   /r_k**2

        bin(k_bin)=bin(k_bin) + 
     $  (power(i,j,k)**2+power1(i,j,k)**2)/fac**4

         bin_phi(k_bin)=bin_phi(k_bin) +
     $ (powerphi(i,j,kp)**2+powerphi1(i,j,kp)**2)/fac**4

        nmodes(k_bin) = nmodes(k_bin) + 1
                  
          endif
        enddo
       enddo 
       enddo
       enddo
      close(1)
      close(2)
      close(3)
      close(4)
      close(5)
      close(9)
      close(7)
      close(8)

       deallocate(powervx, powervx1,powervy, powervy1)
       deallocate(powervz, powervz1, power, power1)

        allocate(datafb(npt,npt,npt))
        call  fftb(datafb, isign2)

       deallocate(powerphi, powerphi1)  

        open (1,file=trim(outfile_grids),FORM="unformatted")
        WRITE(1)datafb(1:npt,1:npt,1:npt/4)
        WRITE(1)datafb(1:npt,1:npt,npt/4+1:npt/2)
        WRITE(1)datafb(1:npt,1:npt,npt/2+1: npt*3/4)
        WRITE(1)datafb(1:npt,1:npt,npt*3/4+1:npt)
        write(*,*)datafb(1:npt,1,1)
       close(1)
      deallocate(datafb)  
 
c writing out power spectrum
        do i=1,num_bin
          poww=0.0
          poww_un=0.0
          ak=exp((i-0.5)/binfac+log(k_min))
          if (nmodes(i).gt.0) then
          poww=bin(i)/nmodes(i)
          poww_phi=bin_phi(i)/nmodes(i)
          endif 
          fact=2*twopi*ak**3./(twopi/side)**3.
          SFac=boxscale**3
       write(14,'(5e15.6)')ak, poww*fact*SFac, 
     $  poww*side**3*SFac,
     $ poww_phi*fact*SFac, poww_phi*side**3*SFac
      enddo
      close(14)
      print*,'Written out power spectrum '
      end


  
          function Ez(z,om)
          implicit none
          real ov, Ez,om, z 
          Ez=sqrt(om*(1.0+z)**3+(1.0-om))
          return
          end function Ez

          function Hz(z,om)
          implicit none
          real :: ov,Hz, Ez, Ho, om, z
          ov=1.0-om
          Ho=100.0
          Hz=Ho*Ez(z,om)
          return
          end function Hz


          function omz(z,om)
          implicit none
          real  om,z,omz, Ez
          omz=om*(1.0+z)**3/Ez(z,om)**2
          return
          end function omz

          function Dz(z, om)
          implicit none
          real  Dz, z, gz, om,omz,ovz
          ovz=1.0-omz(z,om)
          Dz=gz(z,om)/gz(0.0,om)/(1.0+z)
          return 
          end function Dz

           function gz(z, om)
           implicit none
           real :: gz, z, om,omz, ovz
           ovz=1.0-omz(z,om)
           gz=5.0/2.0*omz(z,om)/(omz(z,om)**(4.0/7.0)
     $     -ovz+(1.0+omz(z,om)/2)*(1.0+ovz/70.0))
           return
           end function gz


