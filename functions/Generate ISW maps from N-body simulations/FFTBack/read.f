CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
C     Read in gadget file format                                    C
C                                                                   C
CCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCCC
      subroutine read_gadget(no_sim,filenum,npart,aa,side)
       use common 
      implicit none

      include 'block.inc'
      include 'ngrid.inc' 
      include 'nlevels.inc'
      real*8 qsum
      integer nmax,nump,jjj
      character*3 no_sim,filenum
      real fjfac,qlev,flfac,fkfac
 
      character*160 fname 
      character*800 path,base,tt 
      double precision mtotal 
      integer i,j,k,l              
      real*8 aa, zz
      real  side, ntotal
      integer npart
      integer np(6),nall(8)                
      real*8 massarr(6), tt2(3)     
      real*8 expansion
      integer*4 flagsfr,flagfeedback,flagcooling
      integer*4 NumFiles
      real*8 BoxSize, Box
      real*8 Omega0,OmegaLambda,HubbleParam         
      character unused(256-6*4-6*4-6*8-2*8-4*4-4*8) 
      integer  tt1(2), pt_id
      real no_num
      real tt3(6)
       integer :: unused_in(64-6-12-2-2-1-1-6-1-1-2-2-2-2)
     
ccccc reading the simulation
      path='/disk11/salam/MDPL2/DM/snap'//no_sim//'/'
 
       read(no_sim,'(f3.1)')no_num    
       write(*,*)no_num
     
       allocate(pden(npt,npt, npt), pdenvx(npt,npt,npt))
       allocate(pdenvy(npt,npt, npt), pdenvz(npt,npt,npt))
          pden(:,:,:)=0.0
         pdenvx(:,:,:)=0.0
         pdenvy(:,:,:)=0.0
         pdenvz(:,:,:)=0.0

      print*,'Zeroing all grids '

       do jjj=0,  0
          if (jjj .lt.10) then
             write(tt,'(i1)')jjj
          endif  
        if (jjj .lt.100 .and. jjj .ge. 10)then
                    write(tt,'(i2)')jjj
          endif
         if (jjj .lt.1000 .and. jjj .ge. 100)then
                       write(tt,'(i3)')jjj
                    endif
         if (jjj .ge.1000) then
         write(tt,'(i4)')jjj
        endif
      npart=0
       fname=trim(path)//'snap_'//no_sim//'.'//tt
       print*,fname
      open(1,file=fname, form='unformatted', convert='little_endian')
      read(1)np,  massarr, aa, redshift, tt1, nall,Box, Omega0, 
     $           OmegaLambda, HubbleParam, unused_in
      print*, np,  massarr, aa, redshift, tt1, nall,Box, Omega0, 
     $           OmegaLambda, HubbleParam, unused_in

      write(*,*), 'particle number in the file ', np(2)
      ntotal=38403.**3
      write(*,*),  'total number of particles ', ntotal !nall(2)
      write(*,*), 'total number of files,', nall(8)
      write(*,*), 'particle mass [M_Sun], ', massarr(2)
      write(*,*), 'a, z,', aa,  redshift
!      print*, nall
      BoxSize=Box
      write(*,*), 'BoxSize/1000.0, Omega0, OmegaL', 
     $ BoxSize, Omega0, OmegaLambda
       write(*,'(a18,F6.4)') 'Hubble Parameter : ',HubbleParam
    
       npart=np(2)
      side = BoxSize/boxscale
       print*, 'npart=', npart, 'side=', side
c         read(1)(xxx(i),yyy(i),zzz(i),  i=1,npart) 
c         read(1)(vx(i),vy(i),vz(i),  i=1,npart)
      close(1)

      write(*,*) HubbleParam
      ns = npart
      enddo

      end


      







