      subroutine fftb(datafb, isign)
       use common
!!!!!       use FFTBack
      include 'ngrid.inc'
      real :: datafb(npt, npt,npt)
      real, allocatable :: data(:) 
      integer nd(3), isign
     
       allocate(data(nn))
c        do i=1,nn
c          data(i) = 0.0
c        enddo
           data=0.0

	print*,'Data zeroed '
c
c Now grid point data into npt^3 box
c  

        do j3=-npt/2+1,npt/2
         do j2=-npt/2+1,npt/2
          do j1=-npt/2+1,npt/2
            i1 = j1+1
            i2 = j2+1
            i3 = j3+1
            if (i1 <= 0) i1 = i1+npt
            if (i2 <= 0) i2 = i2+npt
            if (i3 <= 0) i3 = i3+npt
            iind= 2*((i1-1) + npt*(i2-1) + npt*npt*(i3-1))+1
            data(iind)  = powerphi(j1,j2,j3)
            data(iind+1)  = powerphi1(j1,j2,j3)
          enddo
         enddo
        enddo

        print*,'Setting up the FFT '
c   Set up the FFT

c	isign=1
	nd(1)=npt
	nd(2)=npt
	nd(3)=npt
	nnn=3

c        call fourn(data,nd,nnn,isign)
         call ffft(data,nd,nnn,isign)
         print*,'FFT Done'  
        do j3=1,npt
         do j2=1,npt
          do j1=1,npt
             
            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            dx = data(iind)
            dy = data(iind+1)
            datafb(j1,j2,j3)=dx


          enddo
         enddo
        enddo

             deallocate(data)

	return
	end

