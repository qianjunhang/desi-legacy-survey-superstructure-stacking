;;;; Reading the time derivative of the gravitational potential phidot
;;;; in 3D
;;;; Cartesian grids (output from FFTBack), generate full sky healpix
;;;; map via interpolation
;;;; contact: Yan-Chuan Cai at, cai@roe.ac.uk

pro  fullskyMapofISW
 COMMON omega,  om, ov
 common normalization, norm
 
om=0.307115
ov=1.0-om
norm=D0(1.0e-10) 
print, 'norm', norm
Ho=100.0
 
Lbox=1000.0d
Nr=8  ;Number of reprecants
ngrid=1000L 
resolution=256L
StepSize=1.0 ; The dimationless step for integration along the line-of-sight. multiply by a factor of Lbox/ngrid will give you true units
;====================================

OPENR, lun, 'zlist.txt', /GET_LUN
Nsnapshot0=file_lines('zlist.txt') 
Zsnap0=dblarr(Nsnapshot0) 
asnap0=dblarr(Nsnapshot0)
for i=0, Nsnapshot0-1 do begin
readf, lun, a
Zsnap0[i]=1.0/a-1.0
asnap0[i]=a
print,i, Zsnap0[i], asnap0[i]
endfor
FREE_LUN, lun  

sample=1
Nsnapshot=Nsnapshot0/sample
;sampling rate 
Zsnap=dblarr(Nsnapshot)
asnap=dblarr(Nsnapshot) 
Order=lindgen(Nsnapshot)
ct=0L
for i=0, Nsnapshot0-1, sample do begin
Zsnap[ct]=Zsnap0[i]
asnap[ct]=asnap0[i]
Order[ct]=i
ct=ct+1
endfor


LinearFactorSnap=dblarr(Nsnapshot) ; linear factor at snapshots
LinearFactorSnap=phidot(Zsnap)

ComDistSnap=dblarr(Nsnapshot)  
ComDistSnap=ComovingDistance(Zsnap) ; comoving Distance at snapshots

for i=0, Nsnapshot-1 do begin
print, i, Order[i], Zsnap[i],ComDistSnap[i]
endfor

print, 'Zsnap', Zsnap
print, 'ComDistance Snap', ComDistSnap
print, 'LinearFactorSnap', LinearFactorSnap
print, 'hzSnap', hz(Zsnap) 

;===============================================================================================
  
nn=resolution^2*12L   ; Number of Pixels
NestIndex=lindgen(nn)
theta=dblarr(nn)
vector=dblarr(nn)


Radius=(ngrid*(Nr*1.0)) ;The dimationless radius you will integrate
Nstep=FIX(Radius/StepSize) ;Number of steps for integration
print, 'StepSize=', StepSize
print, 'Radius=', Radius*Lbox/ngrid, ' Mpc h^-1'
print, 'Nstep=', Nstep
 
PixelSize=(4.0*!PI/12.0/resolution^2)^0.5*180.0/!PI*60.0
print, 'PixelSize=', PixelSize, ' arcmin'
if(PixelSize gt 60.0)then begin 
PixelSize=PixelSize/60.0
print, 'PixelSize=', PixelSize, ' Degree'
print, 'Warning !!! Pixelize > 1 deg!!!'
endif  

zi=dblarr(Nstep)
comdist=lindgen(Nstep)*StepSize*Lbox/ngrid
LinearFactor=dblarr(Nstep)

ijk=lindgen(nn,3)
ijk0=lindgen(nn,3)
ijkd=lindgen(nn,3)
ijku=lindgen(nn,3)

xyz=dblarr(nn,3)
vector3D=dblarr(nn,3)
int0=dblarr(nn,8)
ll=dblarr(nn); ll is the comoving distance of ijk from the observer
fac=dblarr(nn)
fac_low=dblarr(nn)
fac_up=dblarr(nn)
int_up=dblarr(nn,8)
int_low=dblarr(nn,8)

Dist_up =dblarr(nn)  ; Distance of two snapshots for interpolating.
Dist_low =dblarr(nn)
lambda_up=dblarr(nn) ; Distance of the ray to the two nearby snapshots
lambda_low=dblarr(nn)

for kkk=1, 20 do begin

; locating the low bound and high bound of the integration
Distance_low_bound=kkk*100.0
Distance_up_bound=(kkk+1)*100.0   

Step_low=value_locate(comdist,Distance_low_bound)
Step_up=value_locate(comdist,Distance_up_bound)

N_low=value_locate(ComDistSnap,Distance_low_bound)
N_up=value_locate(ComDistSnap,Distance_up_bound)+1

intensity=dblarr(nn)
intensity[*]=0.0d
print, 'Distance_low_bound  ',Distance_low_bound
print, 'Distance_up_bound  ', Distance_up_bound
print, 'Step_low  ', Step_low
print, 'Step_up  ', Step_up
print, 'ComDistSnap(N_low)  ', ComDistSnap(N_low)  
print, 'ComDistSnap(N_up)  ', ComDistSnap(N_up)
print, 'Snapshot_low  ', N_low, Order[N_low], Zsnap[N_low]
print, 'Snapshot_up  ', N_up, Order[N_up], Zsnap[N_up]
if((N_up-N_low) gt 5)then print, 'Warning!!! More than 5 snapshots will be used.'

Snapstart=N_low
Delta_snapshot=N_up-N_low

;!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
pix2ang_nest, resolution, NestIndex, theta, phi ; Convert pixel centers  into (theta, phi) and then to a vectors
ang2vec, theta, phi, vector 


i=0L & j=0L & k=0L & a=0.0d & b=0.0d & c=0.0d &  d=0.0d  & t=0.0d  & m=1000000
 
nz=10000L
Dis=dblarr(nz)
Ztable=dblarr(nz)
Ztable=lindgen(nz)*0.001d
Dis=ComovingDistance(Ztable)
zi= INTERPOL(Ztable, Dis, comdist)  
LinearFactor=phidot(zi)

intOrigin=fltarr(ngrid,ngrid,ngrid,Delta_snapshot+1) 

for i=0, Delta_snapshot do begin
ii=i+Snapstart

int3D1=fltarr(ngrid,ngrid,ngrid/4)
int3D2=fltarr(ngrid,ngrid,ngrid/4)
int3D3=fltarr(ngrid,ngrid,ngrid/4)
int3D4=fltarr(ngrid,ngrid,ngrid/4)

file='../FFTBack/dataGrids/Gridsa'+strtrim(string(asnap[ii], format='(f7.5)'),2)+'_N'+strtrim(string(ngrid, format='(I4)'),2)+'_L'+strtrim(string(Lbox, format='(f5.0)'),2)       

ISW_factor=3.0*Ho^3*hz(Zsnap[ii])/(3.0e5)^3*om*1.0e6*2.725  
;; c=3.0e5 km/s, T_CMB=1.0e6*2.725 muK

OPENR, lun, file, /GET_LUN, /f77_unformatted;, /swap_endian
readu, lun, int3D1
readu, lun, int3D2
readu, lun, int3D3
readu, lun, int3D4
FREE_LUN, lun  
intOrigin[*,*,0:ngrid/4-1,i]=int3D1[*,*,0:ngrid/4-1]*ISW_factor
int3D1=0.0
intOrigin[*,*,ngrid/4:ngrid/2-1,i]=int3D2[*,*,0:ngrid/4-1]*ISW_factor
int3D2=0.0
intOrigin[*,*,ngrid/2:ngrid*3/4-1,i]=int3D3[*,*,0:ngrid/4-1]*ISW_factor 
int3D3=0.0
intOrigin[*,*,ngrid*3/4:ngrid-1,i]=int3D4[*,*,0:ngrid/4-1]*ISW_factor 
int3D4=0.0

endfor 

print, 'read done'
print, 'Step_low, Step_up', Step_low, Step_up
for j=Step_low, Step_up do begin
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
vector3D=vector*StepSize*j 
ijk=Fix(vector*StepSize*j) 

iii=where(vector le 0) 

ijk0=(ijk+ngrid*Nr*2) mod ngrid 
 
ijkd=ijk0
ijku=(ijk0+1) mod ngrid
ijku[iii]=(ijk0[iii]-1+ngrid*Nr*2) mod ngrid

ll[*]=(vector3D[*,0]^2+vector3D[*,1]^2+vector3D[*,2]^2)^0.5*Lbox/ngrid

zlocate=value_locate(comdist,ll)
;zlocate gives the corresponding redshift zi for ll

Snaplocate=value_locate(Zsnap,zi(zlocate))
;Snaplocate  gives the corresponding lower redshift snapshot zi locate
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
fac_up=LinearFactorSnap(Snaplocate+1)
fac_low=LinearFactorSnap(Snaplocate)
fac=LinearFactor(zlocate)
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
Dist_up=ComDistSnap(Snaplocate+1)
Dist_low=ComDistSnap(Snaplocate)
Delta_Dist=Dist_up-Dist_low
lambda_up=(comdist(zlocate)-Dist_low)/Delta_Dist
lambda_low=(Dist_up-comdist(zlocate))/Delta_Dist
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


int_up[*,0]=intOrigin[ijkd[*,0], ijkd[*,1], ijkd[*,2], Snaplocate+1-Snapstart]
int_low[*,0]=intOrigin[ijkd[*,0], ijkd[*,1], ijkd[*,2],Snaplocate-Snapstart]

int_up[*,1]=intOrigin[ijku[*,0], ijkd[*,1], ijkd[*,2], Snaplocate+1-Snapstart]
int_low[*,1]=intOrigin[ijku[*,0], ijkd[*,1], ijkd[*,2],Snaplocate-Snapstart]

int_up[*,2]=intOrigin[ijkd[*,0], ijku[*,1], ijkd[*,2], Snaplocate+1-Snapstart]
int_low[*,2]=intOrigin[ijkd[*,0], ijku[*,1], ijkd[*,2],Snaplocate-Snapstart]

int_up[*,3]=intOrigin[ijkd[*,0], ijkd[*,1], ijku[*,2], Snaplocate+1-Snapstart]
int_low[*,3]=intOrigin[ijkd[*,0], ijkd[*,1], ijku[*,2],Snaplocate-Snapstart]

int_up[*,4]=intOrigin[ijku[*,0], ijku[*,1], ijkd[*,2], Snaplocate+1-Snapstart]
int_low[*,4]=intOrigin[ijku[*,0], ijku[*,1], ijkd[*,2],Snaplocate-Snapstart]

int_up[*,5]=intOrigin[ijku[*,0], ijkd[*,1], ijku[*,2], Snaplocate+1-Snapstart]
int_low[*,5]=intOrigin[ijku[*,0], ijkd[*,1], ijku[*,2],Snaplocate-Snapstart]

int_up[*,6]=intOrigin[ijkd[*,0], ijku[*,1], ijku[*,2], Snaplocate+1-Snapstart]
int_low[*,6]=intOrigin[ijkd[*,0], ijku[*,1], ijku[*,2],Snaplocate-Snapstart]

int_up[*,7]=intOrigin[ijku[*,0], ijku[*,1], ijku[*,2], Snaplocate+1-Snapstart]
int_low[*,7]=intOrigin[ijku[*,0], ijku[*,1], ijku[*,2],Snaplocate-Snapstart]

xyz=abs(vector3D-ijk)

int0[*,0]=((int_up[*,0]*lambda_up/fac_up+int_low[*,0]*lambda_low/fac_low)*fac[*])*(1.0-xyz[*,0])*(1.0-xyz[*,1])*(1.0-xyz[*,2])
int0[*,1]=((int_up[*,1]*lambda_up/fac_up+int_low[*,1]*lambda_low/fac_low)*fac[*])*xyz[*,0]*(1.0-xyz[*,1])*(1.0-xyz[*,2])
int0[*,2]=((int_up[*,2]*lambda_up/fac_up+int_low[*,2]*lambda_low/fac_low)*fac[*])*(1.0-xyz[*,0])*xyz[*,1]*(1.0-xyz[*,2])
int0[*,3]=((int_up[*,3]*lambda_up/fac_up+int_low[*,3]*lambda_low/fac_low)*fac[*])*(1.0-xyz[*,0])*(1.0-xyz[*,1])*xyz[*,2]
int0[*,4]=((int_up[*,4]*lambda_up/fac_up+int_low[*,4]*lambda_low/fac_low)*fac[*])*xyz[*,0]*xyz[*,1]*(1.0-xyz[*,2])
int0[*,5]=((int_up[*,5]*lambda_up/fac_up+int_low[*,5]*lambda_low/fac_low)*fac[*])*xyz[*,0]*(1.0-xyz[*,1])*xyz[*,2]
int0[*,6]=((int_up[*,6]*lambda_up/fac_up+int_low[*,6]*lambda_low/fac_low)*fac[*])*(1.0-xyz[*,0])*xyz[*,1]*xyz[*,2]
int0[*,7]=((int_up[*,7]*lambda_up/fac_up+int_low[*,7]*lambda_low/fac_low)*fac[*])*xyz[*,0]*xyz[*,1]*xyz[*,2]

intensity[*]=total(int0,2)*StepSize*Lbox/ngrid+intensity[*]
print, 'StepSize*Lbox/ngrid',StepSize*Lbox/ngrid
endfor

title=TeXtoIDL("PixelSize=")+string(PixelSize, format='(f5.2)')+TeXtoIDL("', CubeSize=")+ $
string(Lbox/ngrid,  format='(f4.2)')+TeXtoIDL("Mpc h^{-1}, ")+ TextoIDL("\Deltar_c=")+ $
string(StepSize*Lbox/ngrid, format='(f5.2)')+TeXtoIDL("Mpc h^{-1}, r_c=")+ $
string(Distance_low_bound, format='(I4)')+'-'+string(Distance_up_bound, format='(I4)') $
+TeXtoIDL("Mpc h^{-1}")

bound=strtrim(string(Distance_low_bound, format='(I4)'),2)+'.'+strtrim(string(Distance_up_bound, format='(I4)'),2)

outfits='./Maps/InterN'+strtrim(ngrid,2)+'Nr'+strtrim(resolution,2)+'R'+bound+ $
'dR'+strtrim(string(StepSize*Lbox/ngrid, format='(f5.2)'),2)+'.fits'
print, outfits

outps1='./Maps/InterN'+strtrim(ngrid,2)+'Nr'+strtrim(resolution,2)+'R'+bound+ $
'dR'+strtrim(string(StepSize*Lbox/ngrid, format='(f5.2)'),2)+'.1.ps'

maxin=max(intensity)
minin=min(intensity)
write_fits_cut4,outfits, NestIndex, intensity, /Nested,nside=resolution,extension=0,coordsys='G'
mollview, outfits,min=minin,max=maxin, units=TeXtoIDL("\muK"), title=title, ps=outps1, graticule=[30,30], /preview
print, max(intOrigin),min(intOrigin)
intOrigin=0.0
intensity=0.0
endfor


end 


function phidot, z
 COMMON omega
omz=om*(1.0+z)^3./EzEz(z)
return, hz(z)*Dz(z)*(1.0-growth(z))
end

function growth, z
 COMMON omega
a=1.0/(1.0+z)
return, Xi(z)^(-1.0)*(ov*(1.0+z)^(-2.0)-om*(1.0+z)/2.0)-1.0+(1.0+z)^(-1.0)*Xi(z)^(-1.5)/QROMB('DzInteger', 1.0e-5, a, /DOUBLE)  
end


FUNCTION Xi, z
 COMMON omega
return, 1.0+om*z+ov*((1.0+z)^(-2.0)-1.0)
END  

function DzInteger, a
 COMMON omega
z=1.0/a-1.0
return, 1.0/hz(z)^3.0/a^3
end


function hz, z
 COMMON omega
return, EzEz(z)^0.5
end

function Dz, z
 COMMON omega
 common normalization
a=1.0/(1.0+z)
DzI=(QROMB('DzInteger', 1.0e-5,a, /DOUBLE))
return, hz(z)*DzI/norm
end

function D0, z
 COMMON omega
 common normalization
a=1.0/(1.0+z)
return, hz(z)*(QROMB('DzInteger', 1.0e-5,a, /DOUBLE))
end

function Inter, y1,y2, x1, x2, x
a=(y1-y2)/(x1-x2)
b=y1-a*x1
y=a*x+b  
return, y
end
 
function EzEz,  z
 COMMON omega
return, om*(1.0+z)^3.+ov
end

function hzInteger, z
 COMMON omega
return, 1.0/EzEz(z)^0.5
end

function ComovingDistance, z
 COMMON omega
c=299792.5
h=100.0
return, c/h*(QROMB('hzInteger', 1.0e-7,z, /DOUBLE))
end


