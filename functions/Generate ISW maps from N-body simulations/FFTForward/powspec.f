        module common
        real, allocatable ::pden(:,:,:), pdenvx(:,:,:)
        real, allocatable ::pdenvy(:,:,:), pdenvz(:,:,:)
       real, allocatable :: power(:,:,:), power1(:,:,:)
       real, allocatable :: powervx(:,:,:), powervx1(:,:,:)    
       real, allocatable :: powervy(:,:,:), powervy1(:,:,:)
       real, allocatable :: powervz(:,:,:), powervz1(:,:,:)
        end module common

        use common
      character*128 infile,outfile
      character*70 outfile_grids, base
      integer iargc
      include 'block.inc'
      include 'ngrid.inc'
      include 'nlevels.inc'
 
      real*8 bin(num_bin), bin_un(num_bin)
      real k_min,k_max,binfac,kk,p_es
      real koffset,fact,ak,twopi,poww,poww_un,shotlimit,conv 
      character*3  no_sim,no_file
      character*4  no_z, cnpt
      character*5  cside
      integer k_bin,isub, isign, isign2
      parameter (twopi=2.*3.1415926)
      parameter (Ho=100.0)
      parameter (om=0.307115)
      real SFac
      real phi, Lfac, Hz, z, omz, Dz, fac2
      real kx,ky,kz, nk

      isign=1
      isign2=-1      
      call getarg(1,no_sim)
      write(6,*) no_sim

      call read_gadget(no_sim,no_file,npart,aa,side)

      print*,'Data read ...'
      z=redshift
      write(*,*)'z,      Hz,      omz,     Dz,     boxscale'
      write(*,*)z,Hz(z,om), omz(z,om), Dz(z,om), boxscale
 
      if(z<10.0)then
      write(unit=no_z,fmt='(f4.2)')z
      end if
        
      if(z>=10.0)then
      write(unit=no_z,fmt='(f4.1)')z
      end if
      
       if(npt<1000)then
       write(unit=cnpt, fmt='(I3)')npt
       endif

       if(npt>=1000)then
       write(unit=cnpt, fmt='(I4)')npt
       endif

     
       if(side<1000.0)then
       write(unit=cside, fmt='(f4.0)')side
       endif

       if(side>=1000.0)then
       write(unit=cside, fmt='(f5.0)')side
       endif
       print*, cnpt, cside

         base=trim(trim(no_sim)//'_N'//trim(cnpt)//'_L'//trim(cside))

      outfile='./data/PSphiL1a'//base
      outfile_grids='./dataGrids/Gridsa'//base

      open (14,file=outfile,status='unknown')
 
      call fft(isign)
       open (1,file='./dataGrids/power.'//base,FORM="unformatted")
       WRITE(1)power(:,:,-npt/2+1:-npt/4)
       WRITE(1)power(:,:,-npt/4+1:0)
       WRITE(1)power(:,:,1:npt/4)
       WRITE(1)power(:,:,npt/4+1:npt/2)
        close(1)
       open (1,file='./dataGrids/power1.'//base,FORM="unformatted")
       WRITE(1)power1(:,:,-npt/2+1:-npt/4)
       WRITE(1)power1(:,:,-npt/4+1:0)
       WRITE(1)power1(:,:,1:npt/4)
       WRITE(1)power1(:,:,npt/4+1:npt/2)
        close(1)
       
cccccccccccccccccccccccccccccc

          call fftvx(isign)
       open (1,file='./dataGrids/powervx.'//base,FORM="unformatted")
       WRITE(1)powervx(:,:,-npt/2+1:-npt/4)
       WRITE(1)powervx(:,:,-npt/4+1:0)
       WRITE(1)powervx(:,:,1:npt/4)
       WRITE(1)powervx(:,:,npt/4+1:npt/2)
        close(1)
       deallocate(powervx)
       open (1,file='./dataGrids/powervx1.'//base,FORM="unformatted")
       WRITE(1)powervx1(:,:,-npt/2+1:-npt/4)
       WRITE(1)powervx1(:,:,-npt/4+1:0)
       WRITE(1)powervx1(:,:,1:npt/4)
       WRITE(1)powervx1(:,:,npt/4+1:npt/2) 
        close(1)
       deallocate(powervx1)

       call fftvy(isign)
       open (1,file='./dataGrids/powervy.'//base,FORM="unformatted")
       WRITE(1)powervy(:,:,-npt/2+1:-npt/4)
       WRITE(1)powervy(:,:,-npt/4+1:0)
       WRITE(1)powervy(:,:,1:npt/4)
       WRITE(1)powervy(:,:,npt/4+1:npt/2)
        close(1)
        deallocate(powervy)
       open (1,file='./dataGrids/powervy1.'//base,FORM="unformatted")
       WRITE(1)powervy1(:,:,-npt/2+1:-npt/4)
       WRITE(1)powervy1(:,:,-npt/4+1:0)
       WRITE(1)powervy1(:,:,1:npt/4)
       WRITE(1)powervy1(:,:,npt/4+1:npt/2)
        close(1)
        deallocate(powervy1)

      call fftvz(isign)
       open (1,file='./dataGrids/powervz.'//base,FORM="unformatted")
       WRITE(1)powervz(:,:,-npt/2+1:-npt/4)
       WRITE(1)powervz(:,:,-npt/4+1:0)
       WRITE(1)powervz(:,:,1:npt/4)
       WRITE(1)powervz(:,:,npt/4+1:npt/2)
        close(1)
        deallocate(powervz)
       open (1,file='./dataGrids/powervz1.'//base,FORM="unformatted")
       WRITE(1)powervz1(:,:,-npt/2+1:-npt/4)
       WRITE(1)powervz1(:,:,-npt/4+1:0)
       WRITE(1)powervz1(:,:,1:npt/4)
       WRITE(1)powervz1(:,:,npt/4+1:npt/2)
        close(1)
        deallocate(powervz1)
      end


          function Ez(z,om)
          implicit none
          real ov, Ez,om, z 
          Ez=sqrt(om*(1.0+z)**3+(1.0-om))
          return
          end function Ez

          function Hz(z,om)
          implicit none
          real :: ov,Hz, Ez, Ho, om, z
          ov=1.0-om
          Ho=100.0
          Hz=Ho*Ez(z,om)
          return
          end function Hz


          function omz(z,om)
          implicit none
          real  om,z,omz, Ez
          omz=om*(1.0+z)**3/Ez(z,om)**2
          return
          end function omz

          function Dz(z, om)
          implicit none
          real  Dz, z, gz, om,omz,ovz
          ovz=1.0-omz(z,om)
          Dz=gz(z,om)/gz(0.0,om)/(1.0+z)
          return 
          end function Dz

           function gz(z, om)
           implicit none
           real :: gz, z, om,omz, ovz
           ovz=1.0-omz(z,om)
           gz=5.0/2.0*omz(z,om)/(omz(z,om)**(4.0/7.0)
     $     -ovz+(1.0+omz(z,om)/2)*(1.0+ovz/70.0))
           return
           end function gz


