
       subroutine assign_grids(m,npart, side)
        use common      
       include 'block.inc'
       include 'ngrid.inc'
        real side
        integer ii
         
        scale = 1.
        riscale = 1.
        print*,'m, side', m, side, npart, npt, riscale
        do ii=1,m
         xx = xxx(ii)/side
         yy = yyy(ii)/side
         zz = zzz(ii)/side 
c    xsp=0.xxx. if xx>1.0, then the box will be folded
           xsp = abs(xx*riscale - int(xx*riscale))
           ysp = abs(yy*riscale - int(yy*riscale))
           zsp = abs(zz*riscale - int(zz*riscale))
c    ixx= position of a partical in the grid point
           ixx=int(npt*xsp)
           jxx=int(npt*ysp)
           kxx=int(npt*zsp)
c    actual distance of a partical from its asigned grid
           dx = float(npt)*xsp - float(ixx)
           dy = float(npt)*ysp - float(jxx)
           dz = float(npt)*zsp - float(kxx)

c grid coordinate of the cubic box which contains the partical 
           ixxp = mod(ixx+1,npt) + 1
           jxxp = mod(jxx+1,npt) + 1
           kxxp = mod(kxx+1,npt) + 1

           ixx = ixx + 1
           jxx = jxx + 1
           kxx = kxx + 1

c w1 ~ w8, the eight nearest grid points of the partical
c   CIC assignment scheme
          
           w1 = (1.-dx)*(1.-dy)*(1.-dz)/float(npart)
           w2 = (1.-dx)*(1.-dy)*dz/float(npart)
           w3 = (1.-dx)*dy*(1.-dz)/float(npart)
           w4 = (1.-dx)*dy*dz/float(npart)
           w5 = dx*(1.-dy)*(1.-dz)/float(npart)
           w6 = dx*(1.-dy)*dz/float(npart)
           w7 = dx*dy*(1.-dz)/float(npart)
           w8 = dx*dy*dz/float(npart)

            pden(ixx,jxx,kxx)   = pden(ixx,jxx,kxx)   + w1
            pden(ixx,jxx,kxxp)  = pden(ixx,jxx,kxxp)  + w2
            pden(ixx,jxxp,kxx)  = pden(ixx,jxxp,kxx)  + w3
            pden(ixx,jxxp,kxxp) = pden(ixx,jxxp,kxxp) + w4
            pden(ixxp,jxx,kxx)  = pden(ixxp,jxx,kxx)  + w5
            pden(ixxp,jxx,kxxp) = pden(ixxp,jxx,kxxp) + w6
            pden(ixxp,jxxp,kxx) = pden(ixxp,jxxp,kxx) + w7
            pden(ixxp,jxxp,kxxp)= pden(ixxp,jxxp,kxxp)+ w8


            pdenvx(ixx,jxx,kxx)   = pdenvx(ixx,jxx,kxx)   + w1*vx(ii)
            pdenvx(ixx,jxx,kxxp)  = pdenvx(ixx,jxx,kxxp)  + w2*vx(ii)
            pdenvx(ixx,jxxp,kxx)  = pdenvx(ixx,jxxp,kxx)  + w3*vx(ii)
            pdenvx(ixx,jxxp,kxxp) = pdenvx(ixx,jxxp,kxxp) + w4*vx(ii)
            pdenvx(ixxp,jxx,kxx)  = pdenvx(ixxp,jxx,kxx)  + w5*vx(ii)
            pdenvx(ixxp,jxx,kxxp) = pdenvx(ixxp,jxx,kxxp) + w6*vx(ii)
            pdenvx(ixxp,jxxp,kxx) = pdenvx(ixxp,jxxp,kxx) + w7*vx(ii)
            pdenvx(ixxp,jxxp,kxxp)= pdenvx(ixxp,jxxp,kxxp)+ w8*vx(ii)


            pdenvy(ixx,jxx,kxx)   = pdenvy(ixx,jxx,kxx)   + w1*vy(ii)
            pdenvy(ixx,jxx,kxxp)  = pdenvy(ixx,jxx,kxxp)  + w2*vy(ii)
            pdenvy(ixx,jxxp,kxx)  = pdenvy(ixx,jxxp,kxx)  + w3*vy(ii)
            pdenvy(ixx,jxxp,kxxp) = pdenvy(ixx,jxxp,kxxp) + w4*vy(ii)
            pdenvy(ixxp,jxx,kxx)  = pdenvy(ixxp,jxx,kxx)  + w5*vy(ii)
            pdenvy(ixxp,jxx,kxxp) = pdenvy(ixxp,jxx,kxxp) + w6*vy(ii)
            pdenvy(ixxp,jxxp,kxx) = pdenvy(ixxp,jxxp,kxx) + w7*vy(ii)
            pdenvy(ixxp,jxxp,kxxp)= pdenvy(ixxp,jxxp,kxxp)+ w8*vy(ii)


            pdenvz(ixx,jxx,kxx)   = pdenvz(ixx,jxx,kxx)   + w1*vz(ii)
            pdenvz(ixx,jxx,kxxp)  = pdenvz(ixx,jxx,kxxp)  + w2*vz(ii)
            pdenvz(ixx,jxxp,kxx)  = pdenvz(ixx,jxxp,kxx)  + w3*vz(ii)
            pdenvz(ixx,jxxp,kxxp) = pdenvz(ixx,jxxp,kxxp) + w4*vz(ii)
            pdenvz(ixxp,jxx,kxx)  = pdenvz(ixxp,jxx,kxx)  + w5*vz(ii)
            pdenvz(ixxp,jxx,kxxp) = pdenvz(ixxp,jxx,kxxp) + w6*vz(ii)
            pdenvz(ixxp,jxxp,kxx) = pdenvz(ixxp,jxxp,kxx) + w7*vz(ii)
            pdenvz(ixxp,jxxp,kxxp)= pdenvz(ixxp,jxxp,kxxp)+ w8*vz(ii)
       enddo


       return
       end

       

