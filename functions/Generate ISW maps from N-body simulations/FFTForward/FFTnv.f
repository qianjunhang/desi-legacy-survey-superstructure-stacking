      subroutine fftvx(isign)
      use common
      include 'ngrid.inc'
      real, allocatable :: data(:)
      integer nd(3), isign

       allocate(data(nn))
         data=0.0

c        do i=1,nn
c         data(i) = 0.0
c        enddo

	print*,'Data zeroed '
c
c Now grid point data into npt^3 box
c

        do j3=1,npt
         do j2=1,npt
          do j1=1,npt
            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            data(iind)  = pdenvx(j1,j2,j3)
          enddo
         enddo
        enddo
         deallocate(pdenvx) 
c      print*,'Setting up FFT ',maxval(pd),minval(pd),sum(pd)
c   Set up the FFT

c	isign=1
	nd(1)=npt
	nd(2)=npt
	nd(3)=npt
	nnn=3

c        call fourn(data,nd,nnn,isign)
         call ffft(data,nd,nnn,isign)
         allocate(powervx(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))
        allocate(powervx1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))

        do j3=1,npt
         do j2=1,npt
          do j1=1,npt

            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            dx = data(iind)
            dy = data(iind+1)
c! delta*delta~
c            pow = dx*dx+dy*dy
            i1 = j1-1
            i2 = j2-1
            i3 = j3-1
            if (i1.gt.npt/2) i1 = i1-npt
            if (i2.gt.npt/2) i2 = i2-npt
            if (i3.gt.npt/2) i3 = i3-npt
            powervx(i1,i2,i3)  = dx
            powervx1(i1,i2,i3)  = dy

          enddo
         enddo
        enddo


                deallocate(data)
	return
	end



      subroutine fftvy(isign)
      use common
      include 'ngrid.inc'
      real, allocatable :: data(:)
      integer nd(3), isign

       allocate(data(nn))
                data=0.0
c        do i=1,nn
c          data(i) = 0.0
c        enddo

	print*,'Data zeroed '
c
c Now grid point data into npt^3 box
c

        do j3=1,npt
         do j2=1,npt
          do j1=1,npt
            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            data(iind)  = pdenvy(j1,j2,j3)
          enddo
         enddo
        enddo
             deallocate(pdenvy)
c      print*,'Setting up FFT ',maxval(pd),minval(pd),sum(pd)
c   Set up the FFT

c	isign=1
	nd(1)=npt
	nd(2)=npt
	nd(3)=npt
	nnn=3

c        call fourn(data,nd,nnn,isign)
         call ffft(data,nd,nnn,isign)
        allocate(powervy(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))
        allocate(powervy1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))

        do j3=1,npt
         do j2=1,npt
          do j1=1,npt

            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            dx = data(iind)
            dy = data(iind+1)
c! delta*delta~
c            pow = dx*dx+dy*dy
            i1 = j1-1
            i2 = j2-1
            i3 = j3-1
            if (i1.gt.npt/2) i1 = i1-npt
            if (i2.gt.npt/2) i2 = i2-npt
            if (i3.gt.npt/2) i3 = i3-npt
            powervy(i1,i2,i3)  = dx
            powervy1(i1,i2,i3)  = dy

          enddo
         enddo
        enddo


                deallocate(data)
	return
	end


      subroutine fftvz(isign)
      use common
      include 'ngrid.inc'
      real, allocatable :: data(:)
      integer nd(3), isign

       allocate(data(nn))
         data=0.0

c        do i=1,nn
c          data(i) = 0.0
c        enddo

	print*,'Data zeroed '
c
c Now grid point data into npt^3 box
c

        do j3=1,npt
         do j2=1,npt
          do j1=1,npt
            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            data(iind)  = pdenvz(j1,j2,j3)
          enddo
         enddo
        enddo
            deallocate(pdenvz)
c      print*,'Setting up FFT ',maxval(pd),minval(pd),sum(pd)
c       Set up the FFT

c	isign=1
	nd(1)=npt
	nd(2)=npt
	nd(3)=npt
	nnn=3

c        call fourn(data,nd,nnn,isign)
         call ffft(data,nd,nnn,isign)
        allocate(powervz(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))
        allocate(powervz1(-npt/2+1:npt/2,-npt/2+1:npt/2,-npt/2+1:npt/2))

        do j3=1,npt
         do j2=1,npt
          do j1=1,npt

            iind= 2*((j1-1) + npt*(j2-1) + npt*npt*(j3-1))+1
            dx = data(iind)
            dy = data(iind+1)
c! delta*delta~
c            pow = dx*dx+dy*dy
            i1 = j1-1
            i2 = j2-1
            i3 = j3-1
            if (i1.gt.npt/2) i1 = i1-npt
            if (i2.gt.npt/2) i2 = i2-npt
            if (i3.gt.npt/2) i3 = i3-npt
            powervz(i1,i2,i3)  = dx
            powervz1(i1,i2,i3)  = dy

          enddo
         enddo
        enddo


                deallocate(data)
	return
	end
