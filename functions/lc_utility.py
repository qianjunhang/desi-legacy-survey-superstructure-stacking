#Author: Shadab Alam, salam@roe.ac.uk, January 2021
from __future__ import print_function

import pylab as pl

import numpy as np
import healpy as hp
import numexpr as ne
from astropy.cosmology import Planck15 as cosmo
from astropy.io import fits

import fitsio as F

try:
    import pandas as pd
    pd_flag=1
except:
    pd_flag=0
    print('''pandas not found: you can install pandas from
            https://pypi.python.org/pypi/pandas/0.18.0/#downloads
            The code will work without pandas but pandas will speed up loading large text files
            ''')



def load_txt(fname):
    if(pd_flag==1):
        tb=pd.read_table(fname,delim_whitespace=True,comment='#',header=None,
              dtype=np.float)
        tb=tb.as_matrix()
    else:
        tb=np.loadtxt(fname)
    return tb


def z_rcomv_Mpcbyh(rco_z,rel='z2r',tdic={},zmax=3):
    '''return z2r or r2z dependeing on input
    r is in Mpc/h'''
    
    if('setup' not in tdic.keys()):
        zin=np.linspace(0,zmax,1000)
        dist=cosmo.comoving_distance(zin)
        #distance in Mpc/h
        dist=dist.value*cosmo.h
        tdic={'z':zin,'rco':dist}
        tdic['setup']=True

    if(rel=='z2r'):
        res=np.interp(rco_z,tdic['z'],tdic['rco'])
    elif(rel=='r2z'):
        res=np.interp(rco_z,tdic['rco'],tdic['z'])

    return res, tdic

def xyz_to_polar(xyz):
    '''convert xyz to ra,dec'''
    rco=np.sqrt(np.sum(np.power(xyz,2),axis=1))
    #using y-axis because want rotate theta by 90 degree
    theta=np.arccos(xyz[:,1]/rco)
    if(False):
        ind_pos=xyz[:,0]>0
        theta[ind_pos]=theta[ind_pos]+0.5*np.pi
        theta[~ind_pos]=-theta[~ind_pos]+0.5*np.pi
        
    #using x-axis for phi
    phi=np.arccos(xyz[:,0]/(rco*np.sin(theta)))

    # convert to degree
    theta=np.mod(theta,np.pi)*180.0/np.pi
    phi=np.mod(phi,np.pi)*180.0/np.pi
    
    return rco, theta, phi

def theta_phi_ra_dec(tht,phi):
    """ convert theta,phi to ra/dec """
    ra  = phi/np.pi*180.0
    dec = -1*(tht/np.pi*180.0-90.0)
    return ra,dec

def DM_nearest_snap(scale,zrdic={},sim='MDPL2'):
    '''find the nearest snap to the scale factor for a given sim'''
    if(sim=='MDPL2'):
        snap_dic={'num':np.array([130,112,98]),
                'scale':np.array([1.0,0.67119,0.4921])}
        
    snap_dic['z']=(1.0/snap_dic['scale'])-1
    snap_dic['rco'],_=z_rcomv_Mpcbyh(snap_dic['z'],rel='z2r',tdic=zrdic)
    #find the nearest snapshot to the current scale
    this_z=(1.0/np.float(scale))-1
    this_rco,_=z_rcomv_Mpcbyh(this_z,rel='z2r',tdic=zrdic)
            
    #minimum distance:
    ind_min=np.argmin(np.abs(snap_dic['rco']-this_rco))
    
    return snap_dic['num'][ind_min],snap_dic['scale'][ind_min]
            

class make_lc:
    def __init__(self,input_dir,out_dir,scales_file,Lbox,input_type,sim='MDPL2',write='basic'):
        '''set write to extended if you want an extended catalogue'''
        self.clight= 299792458.
        self.input_dir=input_dir
        self.out_dir=out_dir
        self.scales=scales_file
        self.sim=sim
        
        self.input_type=input_type
        self.Lbox=Lbox
        
        #setup distance-redshift relation
        res,self.zrdic=z_rcomv_Mpcbyh(0,rel='z2r',tdic={},zmax=3)
        
        #load scales
        td=np.loadtxt(self.scales)
        self.boxid=td[:,0]
        self.scales=td[:,1]
        self.z= (1.0/self.scales) -1
        
        self.rco, self.zrdic = z_rcomv_Mpcbyh(self.z,rel='z2r',tdic=self.zrdic)
        
        #decide what catalogue to write
        self.write=write
        self.prop2copy=[]
        
        if(self.input_type=='halos'):
            self.halo={'status':'closed','file':''}
            #other properties to copy
            if(self.write=='extended'):
                self.prop2copy=['Mvir','vrms','rvir','rs']
        elif('gcat' in self.input_type):
            self.colmap={'xx':0,'yy':1,'zz':2,'vx':3,'vy':4,'vz':5,
                        'vrms':6,'Mhalo':7,'flag':8, 'galtype':9}
            if(self.write=='extended'):
                self.prop2copy=['Mhalo','vrms','flag']
        elif(self.input_type=='DM' and self.sim=='RunPB'):   
            self.colmap={'id':0,'xx':1,'yy':2,'zz':3,'vx':4,'vy':5,'vz':6}
            if(self.write=='extended'):
                self.prop2copy=['id']
        elif(self.input_type=='DM' and self.sim=='MDPL2'):   
            self.colmap={'xx':0,'yy':1,'zz':2,'vx':3,'vy':4,'vz':5}
            if(self.write=='extended'):
                self.prop2copy=[]
                
        self.cat={'origin':None}
        self.out_dic={'file':'','status':'close','fout':None}
        
    def create_lc(self,boxid,lowmem=False):
        '''creates the lightcone'''
        
        #first find the shell limit
        stat=self.shell_limit(boxid)
        if(stat==False):
            print('quiting')
            return
        
        #load the galaxy catalog or halo catalog
        if('gcat' in self.input_type):
            self.load_gcat(self.shell['a'])
        elif(self.input_type=='halos'):
            self.load_halos(self.shell['a'])
        elif(self.input_type=='DM'):
            self.load_DM(self.shell['a'])
        
        #select the galaxy with repeating the box
        print('selecting shell')
        self.select_shell(lowmem=lowmem)
        
        if(lowmem==False):
            #convert to sky co-ordinate
            print('converting to sky coordinate')
            stat=self.get_sky()

            #write output in fits file
            #self.write_to_file_astropy()
            if(stat==True):
                self.write_to_file_fitsio()
                
                
        if(self.out_dic['status']=='open'):
            self.out_dic['fout'][-1].write_checksum()
            self.out_dic['fout'].close()
            print('checksum/closed: %s'%(self.out_dic['file']))
            self.out_dic['status']='close'
            self.out_dic['file']=''
            
        
        return
        
    def shell_limit(self,boxid): 
        self.shell={}
        self.shell['boxid']=boxid
        
        ind_id=np.where(self.boxid==boxid)[0]
        if(ind_id.size==0):
            print('invalid boxid=%d'%boxid)
            return False 
        elif(ind_id.size>1):
            print('multiple boxid=%d'%boxid)
            return False
        else:
            ind_id=ind_id[0]
        
        self.shell['a']=self.scales[ind_id]
        self.shell['z']=self.z[ind_id]
        self.shell['rco_mid']=self.rco[ind_id]
        
        #minimum rco
        if(ind_id<self.rco.size-1):
            rco_prev=self.rco[ind_id+1]
            self.shell['rco_min']=0.5*(rco_prev+self.shell['rco_mid'])
        else:
            self.shell['rco_min']=0
            
         
        if(ind_id>0):
            rco_next=self.rco[ind_id-1]
            self.shell['rco_max']=0.5*(rco_next+self.shell['rco_mid'])
        else:
            self.shell['rco_max']=self.shell['rco_min']+100 #fix 100 Mpc/h shell for last one
       
        return True
    
    def load_gcat(self,scale):
        '''loads the galaxy catalog'''
        

        if(self.input_type=='gcat'):
            gcat_file=self.input_dir+'hlist_%7.5f.fits-DECALS-v1.gcat'%scale
        elif(self.input_type=='gcat-pk-1-sigma-1'):
            gcat_file=self.input_dir+'hlist_%7.5f.fits-DECALS-v1-pk-1.00-sigma-1.00.gcat'%scale
        elif(self.input_type=='gcat-interp-pk-1-sigma-1'):
            gcat_file=self.input_dir+'hlist_%7.5f.fits-DECALS-v1-pk-1.00-sigma-1.00.gcat'%scale
        #gcat_file='/disk2/salam/DESI_LegacySurvey_mock/Gcat/hlist_0.50320.fits-DECALS-v1.gcat'
        
        
        
        print('Loading : %s'%gcat_file)
        d     = load_txt(gcat_file)
        self.cat={}
        gtype = d[:,-1]
        idx   = gtype>-1#np.where(gtype==galtype)[0] #only selecting certain galaxies
        ngalbox=idx.size
        
        self.cat['xyz']= np.column_stack([d[idx,0],d[idx,1],d[idx,2]])
        self.cat['vxyz']= np.column_stack([d[idx,3],d[idx,4],d[idx,5]])
        print("using %d/%d galaxies"%(idx.sum(),ngalbox))
        
        for pp, prop in enumerate(self.prop2copy):
            self.cat[prop]=d[idx,self.colmap[prop]]
        
        return
    
    def load_DM(self,scale):
        
        if(self.sim=='RunPB'):
            DM_file=lambda xx: '%sPB00_%6.4f-Particle-0.0005-Real.txt.%s'%(self.input_dir,scale,str(xx).zfill(2))
            #DM_file=lambda xx: '%sPB00_%6.4-Particle-0.0005-Real.txt.%s'%(self.input_dir,scale,str(xx).zfill(2))
            col_map={'id':0,'xx':1,'yy':2,'zz':3,'vx':4,'vy':5,'vz':6}
            nsub_file=64 #number of subfile needs to be loaded
        elif(self.sim=='MDPL2'):
            snap_min,a_min=DM_nearest_snap(scale,zrdic=self.zrdic,sim='MDPL2')
            DM_file=lambda xx: '%ssnap_%s_0.001000.txt'%(self.input_dir,str(snap_min).zfill(3))
            
            print('current:',scale,snap_min,DM_file(0))
            col_map={'xx':0,'yy':1,'zz':2,'vx':3,'vy':4,'vz':5}
            nsub_file=1 #number of subfile needs to be loaded
            
        #if the file loaded is the same then dont reload
        if(self.cat['origin']==DM_file(0)):
            print('already loaded:',self.cat['origin'])
            
            return
        
        self.cat={'origin':DM_file(0)}
        
        for xx in range(0,nsub_file):
            this_file=DM_file(xx)
            d     = load_txt(this_file)
            print('Loaded (%dK) : %s'%(d.shape[0]/1000,this_file))
            
            xyz= np.column_stack([d[:,col_map['xx']],d[:,col_map['yy']],d[:,col_map['zz']]])
            vxyz= np.column_stack([d[:,col_map['vx']],d[:,col_map['vy']],d[:,col_map['vz']]])
            
            if(xx==0):
                self.cat['xyz']= xyz
                self.cat['vxyz']= vxyz
                if('id' in col_map.keys()):
                    self.cat['id']=d[:,col_map['id']]
            else:
                self.cat['xyz']= np.row_stack([self.cat['xyz'],xyz])
                self.cat['vxyz']= np.row_stack([self.cat['vxyz'],vxyz])
                if('id' in col_map.keys()):
                    self.cat['id']=np.append(self.cat['id'],d[:,col_map['id']])
        
        print('total DM:',self.cat['xyz'].shape[0])
        if(False):
            for ii in range(0,3):
                print(ii,self.cat['xyz'][:,ii].min(),self.cat['xyz'][:,ii].max())
                pl.figure();pl.title(str(ii));
                hh=pl.hist(self.cat['xyz'][:,ii],bins=100)
        
        return
    
    
    def load_halos(self,scale):
        '''loads the halo catalog'''
        
        if(self.sim=='RunPB'):
            halo_file=self.input_dir+'halos_%6.4f_wPID.fits'%scale
            Mcut=0
        else:
            halo_file=self.input_dir+'hlist_%7.5f.fits'%scale
            #Mass cut
            Mcut=np.power(10,self.shell['z']+11.0)
        
        
        
        #selection
        sel_str='Mvir>%f && vrms>0'%(Mcut)
        
        #to remove subhalos
        sel_str=sel_str+' && pid==-1'
        
        
        print('Loading : %s\n'%halo_file)
        
        if(self.halo['file']!=halo_file):
            if(self.halo['status']=='open'):
                #close opened file
                self.halo['fh'].close()
                self.halo['status']='close'
                
                
            #open new file
            self.halo['file']=halo_file
            self.halo['fh']=F.FITS(self.halo['file'])
            self.halo['status']='open'

            #now update the selection
            self.halo['ind']=self.halo['fh'][1].where(sel_str)

            self.cat={}
                
        ia=self.halo['ind']
        self.cat['xyz']=np.column_stack([self.halo['fh'][1]['xx'][ia],
                                        self.halo['fh'][1]['yy'][ia],
                                        self.halo['fh'][1]['zz'][ia]])
        self.cat['vxyz']=np.column_stack([self.halo['fh'][1]['vx'][ia],
                                        self.halo['fh'][1]['vy'][ia],
                                        self.halo['fh'][1]['vz'][ia]])
        
        print("using %d halos with selection: %s"%(ia.size,sel_str))
        
        
            
        return
            
    def reset_lc(self):
        self.lc={'rco':np.array([]),
                 'xx':np.array([]),'yy':np.array([]),'zz':np.array([]),
                 'vx':np.array([]),'vy':np.array([]),'vz':np.array([])
                }
        
        for pp, prop in enumerate(self.prop2copy):
            self.lc[prop]=np.array([])
            
        return
    
    def select_shell(self,lowmem=False):
        '''select the galaxies in the shell'''
        
        self.reset_lc()
        
        ntiles=np.int(np.ceil(self.shell['rco_max']/self.Lbox))
        
        print('shell limits:',self.shell['rco_min'],self.shell['rco_max'])
        print('number of tiles: ',ntiles)
        
        max_box={'xx':0,'yy':0,'zz':0}
        for tx in range(-ntiles,ntiles):
            print('tx: %d / %d'%(tx,ntiles))
            if(tx>=0):
                max_box['xx']=tx+1
            else:
                max_box['xx']=tx
                
            for ty in range(-ntiles,ntiles):
                if(ty>=0):
                    max_box['yy']=ty+1
                else:
                    max_box['yy']=ty
                    
                for tz in range(-ntiles,ntiles):
                    if(tz>=0):
                        max_box['zz']=tz+1
                    else:
                        max_box['zz']=tz
                    
                    max_box['rmax']=np.sqrt(max_box['xx']*max_box['xx']+
                                    max_box['yy']*max_box['yy']+
                                    max_box['zz']*max_box['zz'])*self.Lbox
                    
                    if(max_box['rmax']<self.shell['rco_min']):
                        #print(tx,tx,tz,max_box,self.shell['rco_min'],'c')
                        continue
                    #print(tx,ty,tz)
                    
                    rg=np.sqrt(np.power(self.cat['xyz'][:,0]+self.Lbox*tx,2)+
                       np.power(self.cat['xyz'][:,1]+self.Lbox*ty,2)+
                        np.power(self.cat['xyz'][:,2]+self.Lbox*tz,2) )
                    
                    ind=(rg>self.shell['rco_min'])*(rg<self.shell['rco_max'])
                    
                    if(lowmem):#reset lc as it will be written to file to save memory
                        self.reset_lc()
                    
                    tf=[tx,ty,tz]
                    for cc, cax in enumerate(['xx','yy','zz']):
                        self.lc[cax]=np.append(self.lc[cax],
                                    self.cat['xyz'][ind,cc]+self.Lbox*tf[cc])
                        
                    for cc, cax in enumerate(['vx','vy','vz']):   
                        self.lc[cax]=np.append(self.lc[cax],self.cat['vxyz'][ind,cc])
                     
                    self.lc['rco']=np.append(self.lc['rco'],rg[ind])
                    
                    #get other properties
                    for pp, prop in enumerate(self.prop2copy):
                        if('gcat' in self.input_type):
                            self.lc[prop]=np.append(self.lc[prop],self.cat[prop][ind])
                        elif(self.input_type=='halos'):
                            tmp=self.halo['fh'][1][prop][self.halo['ind']]
                            self.lc[prop]=np.append(self.lc[prop],tmp[ind])
                            
                    if(lowmem): #get the sky co-ordinates and write/apppend to file
                        #convert to sky co-ordinate
                        print('converting to sky coordinate')
                        stat=self.get_sky()
                        #print('sizes: ',self.lc['RA'].size)
                        #write output in fits file
                        #self.write_to_file_astropy()
                        if(stat==True):
                            self.write_to_file_fitsio()
                    
        return
            
    def get_sky(self):
        '''get the sky coordinates'''
        
        
        if(self.lc['xx'].size==0):
            print('empty shell!!')
            return False
        
        #obtain redshift
        self.lc['Z'],self.zrdic=z_rcomv_Mpcbyh(self.lc['rco'],rel='r2z',tdic=self.zrdic)
        
        
        tdic={} 
        for cc,cax in enumerate(['x','y','z']):
            tdic['u'+cax]=self.lc[cax+cax]/self.lc['rco']
            tdic['q'+cax]=self.lc['v'+cax]*1000
            #get theta, phi
        tht,phi = hp.vec2ang(np.c_[tdic['ux'],tdic['uy'],tdic['uz']])
        self.lc['RA'],self.lc['DEC']  = theta_phi_ra_dec(tht,phi)
        self.lc['VLOS']= tdic['qx']*tdic['ux'] +tdic['qy']*tdic['uy']+tdic['qz']*tdic['uz']
        self.lc['DZ']  = (self.lc['VLOS']/self.clight )*(1+self.lc['Z'])

        
        return True
    
    def write_to_file_astropy(self):
        outfile=self.out_dir+'lc_mbox_%7.5f.fits'%(self.shell['a'])
        
        cols=[]
        for cc, ca in enumerate(['RA','DEC','Z','DZ','rco']):
            cthis = fits.Column(name=ca     ,array=self.lc[ca]    , format='E')
            cols.append(cthis)
            
        #copy the co-ordinate and velocities
        for cc,cax in enumerate(['x','y','z']):
            #position
            ckey=cax+cax
            cthis = fits.Column(name=ckey,array=self.lc[ckey],format='E')
            cols.append(cthis)
            
            #velocity
            ckey='v'+cax
            cthis = fits.Column(name=ckey,array=self.lc[ckey],format='E')
            cols.append(cthis)

            
        for pp, prop in enumerate(self.prop2copy):
            cthis = fits.Column(name=prop     ,array=self.lc[prop]    , format='E')
            cols.append(cthis)
            

        hdu             = fits.BinTableHDU.from_columns(cols)
        hdr             = fits.Header()
        hdr['abox']  = self.shell['a'] # total number defined as length of ra array
        primary_hdu     = fits.PrimaryHDU(header=hdr)
        hdul            = fits.HDUList([primary_hdu, hdu])

        hdul.writeto(outfile,overwrite=True)
        
        print('written: ',outfile)

    def write_to_file_fitsio(self):
        outfile=self.out_dir+'lc_mbox_%7.5f.fits'%(self.shell['a'])
        
        cols=[]
        for cc, ca in enumerate(['RA','DEC','Z','DZ','rco']):
            cols.append((ca,'>f4'))
            
        #copy the co-ordinate and velocities
        for cc,cax in enumerate(['x','y','z']):
            #position
            ckey=cax+cax
            cols.append((ckey,'>f4'))
        
            
            #velocity
            ckey='v'+cax
            cols.append((ckey,'>f4'))
       
        for pp, prop in enumerate(self.prop2copy):
            cols.append((prop,'>f4'))
                
        #declare a new data array
        data_out=np.zeros(self.lc['RA'].size,dtype=cols)
        
        #copy the info to new arrray
        for tt,tkey in enumerate(data_out.dtype.names):
            #print(tt,tkey,self.lc[tkey].size)
            data_out[tkey]=self.lc[tkey]
            
        #Now decide what to do with the outfile
        if(self.out_dic['status']=='close'):
            self.out_dic['file']=outfile
            self.out_dic['fout']=F.FITS(self.out_dic['file'],'rw')
            self.out_dic['status']='open'
            self.out_dic['fout'].write(data_out)
            print('open: %s, written'%(outfile))
        elif(self.out_dic['file']==outfile):
            #simply append the file
            self.out_dic['fout'][-1].append(data_out)
            print('appended: %s'%outfile)
        elif(self.out_dic['file']!=outfile):
            self.out_dic['fout'][-1].write_checksum()
            self.out_dic['fout'].close()
            self.out_dic['status']='close'
            old_file=self.out_dic['file']
            self.out_dic['file']=outfile
            self.out_dic['fout']=F.FITS(self.out_dic['file'],'rw')
            self.out_dic['status']='open'
            self.out_dic['fout'].write(data_out)
            print('closed:%s \n open:%s , written'%(old_file,outfile))
            
        
        


#To generate the light-cone galaxy catalogue from the cubic galaxy catalogue
sim='MDPL2';input_type='gcat-interp-pk-1-sigma-1' #mock with fitting interpolated bias upto kmax=1

#To generate the light-cone halo catalog from cubic halos
#sim='MDPL2';input_type='halos'

#To generate the light-cone Dark matter catalog from the cubic dark matter
#sim='MDPL2'; input_type='DM'

#sim='RunPB';input_type='halos'
#sim='RunPB';input_type='DM'

#To write additiona properites in the lgiht-cone catalogues
write='extended'

if(sim=='MDPL2'):
    #for this sim boxid =1-32 with 30 missing
    scales_file = '/home/salam/Projects/DESI_LegacySurvey_mock/lightcone-config/MDPL2-scales.txt'
    Lbox= 1000
    if(input_type=='gcat'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/LightCone/'
        out_dir    = lc_dir+'Shells/'
        input_dir   = '/disk2/salam/DESI_LegacySurvey_mock/Gcat/'
    elif(input_type=='gcat-pk-1-sigma-1'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/LightCone-gcat-pk-1-sigma-1/'
        out_dir    = lc_dir+'Shells/'
        input_dir   = '/disk2/salam/DESI_LegacySurvey_mock/Gcat/'
        #/disk2/salam/DESI_LegacySurvey_mock/Gcat/hlist_1.00000.fits-DECALS-v1-pk-1.00-sigma-1.00.hist
    elif(input_type=='gcat-interp-pk-1-sigma-1'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/LightCone-interp-gcat-pk-1-sigma-1/'
        out_dir    = lc_dir+'Shells/'
        input_dir   = '/disk2/salam/DESI_LegacySurvey_mock/Gcat/'
    elif(input_type=='halos'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/LightCone-halos/'
        out_dir    = lc_dir+'Shells/'
        input_dir='/disk2/salam/MDPL2/RockStarHalos/FITS_extended/'
    elif(input_type=='DM'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/MDPL2-LightCone-DM/'
        out_dir    = lc_dir+'Shells/'
        input_dir='/disk2/salam/MDPL2/DM/Subsamp/'
elif(sim=='RunPB'):
    #for this sim boxid =1-9
    scales_file = '/home/salam/Projects/DESI_LegacySurvey_mock/lightcone-config/RunPB-scales.txt'
    Lbox= 1380
    if(input_type=='halos'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/RunPB-LightCone-halos/PB00/'
        out_dir    = lc_dir+'Shells/'
        input_dir='/disk2/salam/RunPB/FITS_rockstar_halos/PB00/'
    elif(input_type=='DM'):
        lc_dir = '/disk2/salam/DESI_LegacySurvey_mock/RunPB-LightCone-DM/PB00/'
        out_dir    = lc_dir+'Shells/'
        input_dir='/disk2/salam/RunPB/SubSamp_ParticleTXT/PB00/'
        


mlc=make_lc(input_dir,out_dir,scales_file,Lbox,input_type,sim=sim,write=write)

#boxid=4
boxid=1
mlc.shell_limit(boxid)
print(mlc.shell)
#mlc.create_lc(boxid,lowmem=True)

print('done')

