#Author: Shadab Alam, salam@roe.ac.uk, January 2021
from __future__ import print_function

import os

import pylab as pl

import numpy as np
import healpy as hp
import numexpr as ne
from astropy.cosmology import Planck15 as cosmo
from astropy.io import fits

import fitsio as F

from astropy.cosmology import Planck15 as cosmo

try:
    import pandas as pd
    pd_flag=1
except:
    pd_flag=0
    print('''pandas not found: you can install pandas from
            https://pypi.python.org/pypi/pandas/0.18.0/#downloads
            The code will work without pandas but pandas will speed up loading large text files
            ''')


import pickle


def dump_save(stuff,filename):
    '''This saves the dictionary and loads it from appropriate files'''
    with open(filename,'wb') as fout:
        pickle.dump(stuff,fout,pickle.HIGHEST_PROTOCOL)
        #json.dump(self.impute, fout, sort_keys=True, indent=3)
    print('written impute ditionary:',filename)
    return 0
def dump_load(filename):
    with open(filename,'rb') as fin:
        stuff=pickle.load(fin)
        #self.impute = json.load(fin)
    #print('loaded impute ditionary:',filename)
    return stuff


def hp_pixelize(ra,dec,nside=256,nest=True,weights=None,
                plots=0,title='',return_pix=False):
    #number of pixel and are per pixel for given nside
    npixel=12*np.power(nside,2)
    area_perpix=4.0*np.pi*np.power(180.0/np.pi,2)/npixel

    #find pixel for each entry
    pixel= hp.ang2pix(nside, ra,dec,nest=nest,lonlat=True)

    hbin=np.linspace(0,npixel,npixel+1)-0.5
    pixel_map,bb=np.histogram(pixel,bins=hbin,weights=weights)

    if(plots==1):
        #pl.figure()
        hp.mollview(pixel_map,nest=nest)#,min=limit[0],max=limit[1])
        pl.title(title)
        
    if return_pix==False:
        return pixel_map#,pixel
    elif return_pix==True:
        return pixel_map,pixel
    
def estimate_Fmass_inhalo(scale=1.0,sim='MDPL2'):
    'simulation detail https://www.cosmosim.org/cms/simulations/mdpl2/'
    
    if(sim=='MDPL2'):
        pbc_halo_dir='/disk2/salam/MDPL2/RockStarHalos/FITS_extended/'
        halo_file=pbc_halo_dir+'hlist_%7.5f.fits'%scale

        mpart=1.51e9
        npart=np.power(3840,3)
        this_z=(1.0/scale) -1
        #Mass cut
        Mcut=np.power(10,this_z+11.0)
    elif(sim=='RunPB'):
        pbc_halo_dir='/disk2/salam/RunPB/FITS_rockstar_halos/PB00/'
        #halo_file=pbc_halo_dir+'hlist_%7.5f.fits'%scale
        halo_file=pbc_halo_dir+'halos_%6.4f_wPID.fits'%scale
        
        mpart=24792717336.1022
        npart=np.power(2048,3)
        #Mass cut
        Mcut=0
    
    
    total_mass=mpart*npart
    
    #selection
    sel_str='Mvir>%f && vrms>0'%(Mcut)
    #to remove subhalos
    sel_str=sel_str+' && pid==-1'
    
    with F.FITS(halo_file) as fin:
        indsel=fin[1].where(sel_str)
        Mhalo=np.sum(fin[1]['Mvir'][indsel])
    
    Fmass_inhalo=Mhalo/total_mass
    
    print(sim,scale,Fmass_inhalo,Mcut)
    return Fmass_inhalo


def growth_factor(z,Omega_m0=0.27):
    a=1/(1+z)
    #Omega_m0=0.3
    Omega_m=Omega_m0*a**(-3)/(Omega_m0*a**(-3)+1-Omega_m0)
    x=(1-Omega_m)**(1/3.0)
    g=x*(1-x**1.91)**0.82+1.437*(1-(1-x**3)**(2/3.0))
    x0=(1-Omega_m0)**(1/3.0)
    g0=x0*(1-x0**1.91)**0.82+1.437*(1-(1-x0**3)**(2/3.0))
    return g/g0

def MDPL2_DM_delta_weight(scale=1.0):
    '''figure out the reweighting needed for MDPL2 DM 
    as per the grwoth rate'''
    
    snap_dic={'num':np.array([130,112,98]),
            'scale':np.array([1.0,0.67119,0.4921])}
        
    snap_dic['z']=(1.0/snap_dic['scale'])-1
    snap_dic['rco']=cosmo.comoving_distance(snap_dic['z'])*cosmo.h
        
    #find the nearest snapshot to the current scale
    this_z=(1.0/np.float(scale))-1
    
    this_rco=cosmo.comoving_distance(this_z)*cosmo.h
            
    #minimum distance:
    ind_min=np.argmin(np.abs(snap_dic['rco']-this_rco))
    
    #now find the growth at the two redshift
    gz_min=growth_factor(snap_dic['z'][ind_min],Omega_m0=cosmo.Om0)
    gz_now=growth_factor(this_z,Omega_m0=cosmo.Om0)
    
    weight=gz_now/gz_min
    return weight

class Make_delta(): #makes the mass weighted over density maps and write it to files along with few other info
    def __init__(self,shell_dir,nside,nest=True,weights='Mvir',sim='MDPL2',overwrite=False,
                 debug=False,verbose=True):
        self.shell_dir=shell_dir
        self.sim=sim
        #if(nside==1024 and weights=='Mvir'):
        #    self.dens_dir=self.shell_dir+'../densmap/'
        if(weights==None):
            self.dens_dir=self.shell_dir+'../densmap_nside%d_w1/'%nside
        else:
            self.dens_dir=self.shell_dir+'../densmap_nside%d_%s/'%(nside,weights)
            
        self.verbose=verbose
        
        self.nside=nside
        self.nest=nest
        self.weights=weights
        self.debug=debug
        self.overwrite=overwrite
        

            
        #generate the info about the shells
        self.generate_shell_info()        

        #generate the density field
        self.generate_delta()

    def generate_shell_info(self):
        '''look at all the files in the directory and generate a sequence of shell
        This also test the shell for continuous overlap in rco'''
        
        if(self.verbose):
            print('collecting information about shells..')
        
        self.shell_file_list=os.listdir(self.shell_dir)
        
        scale_list=[]
        aa2fname={}
        #generate the scale factor for file
        for ll, fname in enumerate(self.shell_file_list):
            scale_list.append(np.float(fname.split('_')[-1][:-5]))
            aa2fname[scale_list[-1]]=fname
            
        #convert to array
        scale_list=np.array(scale_list)
        
        #make sure this is unique and arrange them in descending order
        assert np.unique(scale_list).size==scale_list.size
        
        scale_list.sort()
        scale_list=scale_list[::-1]
        if(self.debug):
            scale_list=scale_list[10:12] #temporary using only two shell for testing
        
        #number of shell
        self.num_shell=scale_list.size
        
        self.shell_dic={}
        #now go over the scale list
        for ii, aa in enumerate(scale_list):
            fname=self.shell_dir+aa2fname[aa]
            self.shell_dic[ii]={'aa':aa,'fname':fname}
            #define the filename for output map
            self.shell_dic[ii]['map_file']='%sdens_map_%s.pkl'%(self.dens_dir,fname.split('/')[-1][:-5])

            if(self.load_shell_dic(ii,verbose=False)):
                continue
                
            with F.FITS(self.shell_dic[ii]['fname']) as ftmp:
                qlist=['rco','Z']
                if('Mvir' in ftmp[1][1].dtype.names):
                    qlist.append('Mvir')
                    
                for prop in qlist:
                    self.shell_dic[ii][prop+'_min']=ftmp[1][prop][:].min()
                    self.shell_dic[ii][prop+'_max']=ftmp[1][prop][:].max()
                    self.shell_dic[ii][prop+'_mean']=ftmp[1][prop][:].mean()
        
        
        #check for continuity
        if(self.verbose):
            print('validating shells for continuity in comoving distance...')
            
        for ii in range(0,max(self.shell_dic.keys())):
            diff=self.shell_dic[ii]['rco_max']-self.shell_dic[ii+1]['rco_min']
            t1=self.shell_dic[ii];t2=self.shell_dic[ii+1]
            msg='rco: %s (%f,%f) <> %s (%f,%f)'%(t1['fname'],t1['rco_min'],t1['rco_max'],
                                                 t2['fname'],t2['rco_min'],t2['rco_max'])
            assert (np.abs(diff)<1), 'discontinuous coverage in the shells: %s'%msg
        
        
        
        return
    

    def load_shell_dic(self,ii,verbose=False):
        #load the map
        if(os.path.isfile(self.shell_dic[ii]['map_file']) and self.overwrite==False):
            self.shell_dic[ii]=dump_load(self.shell_dic[ii]['map_file'])
            if(verbose):
                print('loaded: ',self.shell_dic[ii]['map_file'])
            return True
        else:
            return False
        
    
    def generate_delta(self):
        '''generate the delta field and write to a file'''
        
        if(self.verbose):
            print('generating overdensity maps for shells')
            
        for ii in range(0,self.num_shell):   
            fname=self.shell_dic[ii]['fname']   

            if(self.load_shell_dic(ii,verbose=self.verbose)):
                if('Fmass_inhalo' not in self.shell_dic[ii].keys() and False):
                    self.shell_dic[ii]['Fmass_inhalo']=estimate_Fmass_inhalo(scale=self.shell_dic[ii]['aa'],
                                                                        sim=self.sim)        
                    dump_save(self.shell_dic[ii],self.shell_dic[ii]['map_file'])
                    print('updated: ',self.shell_dic[ii]['map_file'])
                    
                continue
                
            with F.FITS(fname) as ftmp:
                #get the weighted healpix 
                if(self.weights==None):
                    weights=None
                else:
                    weights=ftmp[1][self.weights][:]
                    
                pixel_map=hp_pixelize(ftmp[1]['RA'][:],ftmp[1]['DEC'][:],nside=self.nside,
                                      nest=self.nest,weights=weights,
                                plots=0,title=self.weights,return_pix=False)
                
                #convert to over-density
                self.shell_dic[ii]['densmap']=(pixel_map/pixel_map.mean()) -1
                #hp.mollview(pixel_map,nest=self.nest)#,min=limit[0],max=limit[1])
                
                #write to file self.shell_dic['map_file']
                dump_save(self.shell_dic[ii],self.shell_dic[ii]['map_file'])
                print('written: ',self.shell_dic[ii]['map_file'])
                
            #if(self.debug): 
            #    break; #comment after debugging
            
    def plot_quant_with_z(self,xax='Z',yax='Mvir'):
        '''plot the redshift dependence of certain quantity'''
        
        xval=[]
        if(yax=='Mvir'):
            yval_d={'mean':[],'min':[],'max':[]}
        elif('Fmass_inhalo'):
            yval_d={'':[]}
        else:
            yval_d={'mean':[]}
        
        
        for tt,tkey in enumerate(self.shell_dic.keys()):
            xval.append(self.shell_dic[tkey][xax+'_mean'])
            for pp in yval_d.keys():
                if(pp==''):
                    yval_d[''].append(self.shell_dic[tkey][yax])
                else:
                    yval_d[pp].append(self.shell_dic[tkey][yax+'_'+pp])
                
        if('' in yval_d.keys()):
            pl.plot(xval,yval_d[''],'k-',label=yax)
        else:
            pl.plot(xval,yval_d['mean'],'k-',label=yax)
        
        if(yax=='Mvir'):
            pl.plot(xval,yval_d['min'],'k-.',label=yax+'_min')
            pl.plot(xval,yval_d['max'],'k--',label=yax+'_max')

            pl.yscale('log')
            
        pl.legend(fontsize=12)
            
        pl.xlabel(xax,fontsize=22)
        pl.ylabel(yax,fontsize=22)
        

class make_Kappa():
    #makes the kappa map by integrating the mass weighted density map
    def __init__(self,shell_dir,nside,nest=True,
                 weights='Mvir',omega_m=0.315,
                 overwrite=False,debug=False):
        #declare a make delta object first
        #if density map is not yet created then it will create one
        print('loading over-density object')
        self.mdelta=Make_delta(shell_dir,nside,nest,weights=weights,
                               overwrite=overwrite,debug=debug,verbose=False)
        
        self.omega_m=omega_m
        self.speed_of_light=2.998e5
        
    def lensing_kernel(self,shell):
        KK=3*np.power(cosmo.h*100,2)*self.omega_m/(2.0*np.power(self.speed_of_light,2))
        
        #KK=3/2.*100**2*self.omega_m/self.speed_of_light**2
        
        zcmb=1070
        rls=cosmo.comoving_distance(zcmb)
        #distance in Mpc/h
        rls=rls.value*cosmo.h
        
        #rls=mycosmo.r_comoving(1070,100,omega_m,1-omega_m)
        
        bincen=shell['rco_mean']
        zcen=shell['Z_mean']
        #Lkernel=(rls-bincen)*bincen/rls*(1+zcen)
        Lkernel=(rls-bincen)*bincen*(1+zcen)/rls
        
        #the width of the shell
        dr=shell['rco_max']-shell['rco_min']
        KK=KK*Lkernel*dr
        #in this kernel the distances should be in Mpc/h and hence we need a factor of h^-2
        KK=KK/np.power(cosmo.h,2)
        
        if(self.kernel.size==0):
            self.kernel=np.array([bincen,KK])
        else:
            self.kernel=np.row_stack([self.kernel,np.array([bincen,KK])])
        
        return KK
        
    def lensing_integration(self,method='noFscale',output_shell=False):
        '''This performs the line-of-sight integration
        Since we are estimating lensing from mass weighted halo field we cane use two method to estimate kappa
        method=Fscale , This simply assumes same contribution from lower mass halos and scale the lensing
        by the fraction of mass in the haloes (Fmass_inhalo)
        method=powerlaw_bias: This consider a power law bias model for the density field and 
        estimate the contribution of mass in smaller halo accordingly
        method=w1 no mass wighting is used
        
        output_shell: if Individual shells needs to be written then set it to True'''
        
        self.kernel=np.array([])
        
        outroot=self.mdelta.shell_dir+'../kappa_map_%s_%s_nside%d'%(method,
                                        self.mdelta.weights,self.mdelta.nside)
        if(output_shell):
            shell_dir=outroot+'_shell/'
            if(not os.path.isdir(shell_dir)):
                os.system('mkdir %s'%shell_dir)
                print('created directory: ',shell_dir)
        
        for ii in range(0,self.mdelta.num_shell):
            print(ii,self.mdelta.shell_dic[ii]['aa'])
            if(ii==0):
                self.kappa_map=np.zeros(self.mdelta.shell_dic[ii]['densmap'].size) #to store the kappa map
                
            kernel=self.lensing_kernel(self.mdelta.shell_dic[ii])
            if(method=='Fscale'):
                kernel=kernel/self.mdelta.shell_dic[ii]['Fmass_inhalo']
            elif(method=='noFscale'):
                kernel=kernel
            elif(method=='gzMDPL2_DM'):
                ww=MDPL2_DM_delta_weight(scale=self.mdelta.shell_dic[ii]['aa'])
                kernel=kernel*ww
            
            if(output_shell):#To write the individual shell
                this_shell_file='%skappa_%s.pkl'%(shell_dir,self.mdelta.shell_dic[ii]['aa'])
                #output dictionary
                outdic={'kappa':kernel*self.mdelta.shell_dic[ii]['densmap'],
                       'nside':self.mdelta.nside,
                       'nest':self.mdelta.nest,
                      }
                for prop in ['aa','Z_max', 'Z_mean', 'rco_mean', 'Z_min', 'rco_min', 'rco_max']:
                    outdic[prop]=self.mdelta.shell_dic[ii][prop]
                    
               
                dump_save(outdic,this_shell_file)
                print('written: ',this_shell_file)
                
            self.kappa_map=self.kappa_map+(kernel*self.mdelta.shell_dic[ii]['densmap'])
                
        
        if(self.mdelta.nest==True):
            outfile=outroot+'_nested'
        outfile=outfile+'.pkl'
        
        dump_save({'kappa':self.kappa_map,'nside':self.mdelta.nside,'nest':self.mdelta.nest},outfile)
        print('written: ',outfile)
        
        return
        


        
#MDPL2 DM LC
sim='MDPL2'; shell_dir='/disk2/salam/DESI_LegacySurvey_mock/MDPL2-LightCone-DM/Shells/'

nside=512;weights=None

md=Make_delta(shell_dir,nside,weights=weights,sim=sim,overwrite=False,debug=False,verbose=False)

#md.plot_quant_with_z(xax='Z',yax='Mvir')#yax='Fmass_inhalo')
pl.show()


#This is to make the kappa map
mkap=make_Kappa(shell_dir,nside,nest=True,
                 weights=weights,omega_m=0.315,
                 overwrite=False,debug=False)

mkap.lensing_integration(method='gzMDPL2_DM',output_shell=False)#'noFscale')#'Fscale')

print('done')
