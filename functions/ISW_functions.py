import numpy as np
import scipy
from scipy import spatial
import healpy as hp
import pylab as pl
from astropy.io import fits
import sys
sys.path.insert(0, '/disk1/qhang/Projects/UsefulFunctions/')
import Useful_functions as uf
import pickle

def potential_voids_indices(delta,density_cut=-0.3):
    '''
    delta: of all data
    density_cut: to determine the potential voids
    return: indices of potential voids, sorted according to delta
    '''
    deltaindex=np.arange(len(delta))
    potential_voids=delta<density_cut
    potential_voids=deltaindex[potential_voids]
    sortidx_pot_voids=np.argsort(delta[potential_voids])
    sorted_potential_voids=potential_voids[sortidx_pot_voids]
    return sorted_potential_voids


def simple_void_finder(data,delta,potential_voids,r_bins,shell_width,tolerate=0,print_steps=True):
    '''
    data: [xx,yy,zz]
    potential_voids: indices of the potential voids, sorted according to delta
    r_bins: radius to look at
    return: void centres, radii, first 5 avg_dens as a function of r_bins
    '''
    #tree
    tree = spatial.cKDTree(data)
    iterative_potential_voids=np.copy(potential_voids)
    void_centre_index=[]
    void_radii=[]
    #out_avg_dens=np.zeros((40,5))
    N=0
    while len(iterative_potential_voids)>0:
    #for N in range(5):
        index=iterative_potential_voids[0]
        #void_centre_index=np.append(void_centre_index,index)
        #print(index)
        point=data[index]
        avg_dens=np.zeros(len(r_bins))
        #void_members=[]
        tick=0
        for ii in range(len(r_bins)):
            neighbours=tree.query_ball_point(point,r_bins[ii])
            neighbours_shell=tree.query_ball_point(point,r_bins[ii]+shell_width)
            if len(neighbours)==len(neighbours_shell):
                #This means shell is empty
                #Take avg density from previous shell
                if ii==0:
                    avg_dens[ii]=np.mean(delta[neighbours])
                elif ii>0:
                    avg_dens[ii]=avg_dens[ii-1]
            elif len(neighbours_shell)>len(neighbours):
                inshell_index=np.in1d(neighbours_shell, neighbours)
                #print(neighbours_shell,neighbours)
                #print(inshell_index)
                #shell_index=neighbours_shell[~inshell_index]
                #print(delta[neighbours_shell][~inshell_index])
                avg_dens[ii]=np.mean(delta[neighbours_shell][~inshell_index])
            if avg_dens[ii]>=tolerate:
                if(avg_dens[:ii]<tolerate).all()==True:
                    void_members=np.copy(neighbours_shell)
                    #print((void_members==index).any())
                    void_radii_temp=r_bins[ii]+shell_width/2.
                    tick=1
            if tick==0:
                if ii==(len(r_bins)-1):
                    void_members=np.copy(neighbours_shell)
                    #print((void_members==index).any())
                    void_radii_temp=r_bins[ii]+shell_width/2.
        
        #find potential void centres inside this void
        void_index=np.in1d(iterative_potential_voids,void_members)
        #remove potential void centres
        iterative_potential_voids=iterative_potential_voids[~void_index]
        if N==0:
            void_centre_index=np.append(void_centre_index,index)
            void_radii=np.append(void_radii,void_radii_temp)
            out_avg_dens=np.copy(avg_dens)
        elif N>0:
            #find existing void centres inside this void
            void_index2=np.in1d(void_centre_index,void_members)
            #remove overlapping exisiting void centres
            void_centre_index=np.append(void_centre_index[~void_index2],index)
            #remove the void radii of the exisiting void centre
            void_radii=np.append(void_radii[~void_index2],void_radii_temp)
            #remove the avg_dens of the exisiting void centre
            if len(void_centre_index)==1:
                out_avg_dens=np.copy(avg_dens)
            elif len(void_centre_index)==2:
                out_avg_dens=np.column_stack((out_avg_dens,avg_dens)) 
            elif len(void_centre_index)>2:
                out_avg_dens=np.column_stack((out_avg_dens[:,~void_index2],avg_dens))  
        if print_steps==True:
            print(len(iterative_potential_voids),len(void_centre_index))
        N+=1
        #print(N)
    return void_centre_index,void_radii,out_avg_dens,N

def void_centre(data,data_density,voids,void_radii):
    '''
    data: [xx,yy,zz]
    data_density: density of each point
    voids: indices of the voids
    void_radii: radii of the voids
    return: volume-weighted void centre index in [xx,yy,zz]
    '''
    #tree
    tree = spatial.cKDTree(data)
    
    void_centre=np.zeros((len(voids),3))
    for ii in range(len(voids)):
        index=int(voids[ii])
        point=data[index]
        #void_members=[]
        neighbours=tree.query_ball_point(point,void_radii[ii])
        neighbour_pos=data[neighbours]
        weight=1/data_density[neighbours]
        weight=weight/sum(weight)
        weight=np.outer(weight, np.ones(3))
        
        void_centre[ii,:]=np.sum(neighbour_pos*weight,axis=0)
    return void_centre

#rotational matrix
def rot_x(ang,deg=True):
    if deg==True:
        ang=ang*np.pi/180.
    return np.array([[1,0,0],[0,np.cos(ang),-np.sin(ang)],[0,np.sin(ang),np.cos(ang)]])

def rot_y(ang,deg=True):
    if deg==True:
        ang=ang*np.pi/180.
    return np.array([[np.cos(ang),0,np.sin(ang)],[0,1,0],[-np.sin(ang),0,np.cos(ang)]])

def rot_z(ang,deg=True):
    if deg==True:
        ang=ang*np.pi/180.
    return np.array([[np.cos(ang),-np.sin(ang),0],[np.sin(ang),np.cos(ang),0],[0,0,1]])

def rotate_map(hmap, rot_theta, rot_phi, nest=True, rand_xrot=False, return_index=False):
    """
    Take hmap (a healpix map array) and return another healpix map array 
    which is ordered such that it has been rotated in (theta, phi) by the 
    amounts given.
    """
    nside = hp.npix2nside(len(hmap))

    # Get theta, phi for non-rotated map
    #t,p = hp.pix2ang(nside, np.arange(hp.nside2npix(nside)), nest=True) #theta, phi
    xx,yy,zz = hp.pix2vec(nside, np.arange(hp.nside2npix(nside)), nest=nest)
    
    # Define a rotator
    #r = hp.rotator.Rotator(deg=True, rot=[rot_phi,rot_theta], inv=False, eulertype='X')
    rotmat=np.dot(rot_y(rot_theta),rot_z(-rot_phi))
    
    if rand_xrot==True:
        rand_ang=np.random.uniform(low=0,high=360,size=1)
        rotmat=np.dot(rot_x(rand_ang),rotmat)
    
    rot_xx,rot_yy,rot_zz=hp.rotator.rotateVector(rotmat,xx,yy,zz)
    
    # Get theta, phi under rotated co-ordinates
    #trot, prot = r(t,p)
    #rot_vector=r(vector)
    
    #prot, trot=hp.vec2ang(np.c_[rot_xx,rot_yy,rot_zz], lonlat=True)
    #rotpix=vec2pix(nside=8,xrot,yrot,zrot,nest=True)
    
    rot_pix=hp.vec2pix(nside,rot_xx,rot_yy,rot_zz,nest=nest)
    
    if return_index==True:
        return rot_pix
    elif return_index==False:
        # Interpolate map onto these co-ordinates
        #rot_map = hp.get_interp_val(hmap, trot, prot,lonlat=True)
        rot_map=np.zeros(len(hmap))
        #assert(len(np.unique(rot_pix))==len(rot_pix))
        rot_map[rot_pix]=hmap
        return rot_map

def hp_pixelize(ra,dec,nside=256,nest=True,plots=0,title='',return_pix=False,limit=None):
    #number of pixel and are per pixel for given nside 
    npixel=12*np.power(nside,2)
    area_perpix=4.0*np.pi*np.power(180.0/np.pi,2)/npixel
    
    #find pixel for each entry
    pixel= hp.ang2pix(nside, ra,dec,nest=nest,lonlat=True)
    
    hbin=np.linspace(0,npixel,npixel+1)-0.5
    pixel_map,bb=np.histogram(pixel,bins=hbin)

    if(plots==1):
        pl.figure()
        hp.mollview(pixel_map,nest=nest,min=limit[0],max=limit[1])
        pl.title(title)
    if return_pix==False:   
        return pixel_map#,pixel
    elif return_pix==True:
        return pixel_map,pixel

def circles(centrex,centrey,radius):
    #centre is 
    x=np.linspace(-radius, radius, 20)
    y=np.sqrt(radius**2-x**2)
    x=x+centrex
    y1=y+centrey
    y2=centrey-y
    return x, y1, y2

def change_hpmap_coordinate(hpmap,nside,from_coord,to_coord,nest=False):
    
    r=hp.Rotator(coord=[to_coord,from_coord])
    #convert pixel map to angles
    ipix=np.arange(hpmap.shape[-1])
    
    Gangles=hp.pix2ang(nside, ipix, nest=nest, lonlat=False)
    
    Cangles=r(*Gangles, lonlat=False)
    
    new_pix = hp.ang2pix(nside, *Cangles)
    
    newmap=hpmap[..., new_pix]

    return newmap

def hp_pixelize_intensity(ra,dec,delta,nside=256,nest=True,plots=0,title='',normalize=False):
    #number of pixel and are per pixel for given nside 
    npixel=12*np.power(nside,2)
    area_perpix=4.0*np.pi*np.power(180.0/np.pi,2)/npixel
    
    #find pixel for each entry
    pixel= hp.ang2pix(nside, ra,dec,nest=nest,lonlat=True)
    
    hbin=np.linspace(0,npixel,npixel+1)-0.5
    pixel_map,bb=np.histogram(pixel,bins=hbin)
    
    pixel_intensity=np.zeros(npixel)
    for kk in range(len(pixel)):
        ind=pixel[kk]
        pixel_intensity[ind]+=delta[kk]
    
    ##if pixel_map=0 at any pixel, then intensity would also be zero
    zeroind=pixel_map==0
    pixel_map_non0=np.copy(pixel_map)
    pixel_map_non0[zeroind]=1.
    
    if normalize==True:
        pixel_intensity=pixel_intensity/pixel_map_non0.astype(float)

    if(plots==1):
        pl.figure()
        hp.mollview(pixel_map,nest=nest)
        pl.title(title)
        
        pl.figure()
        #hp.mollview(pixel_intensity_norm,nest=nest)
        hp.mollview(pixel_intensity,nest=nest)
        #pl.title(title)
        
    return pixel_map, pixel_intensity#, pixel_intensity_norm

def simple_void_finder_2Dhealpy(survey_mask,delta_map,nside,nest,potential_voids,r_bins, shell_width,tolerate=0,print_steps=True,overlap=True):
    '''
    data: [ra, dec]
    pixel_map: healpix map of galaxies
    delta_map: healpix map of sum of delta of galaxies in each pixel
    potential_voids: indices of the potential voids, sorted according to delta
    r_bins: radius in radian to look at
    return: void centres, radii, first 5 avg_dens as a function of r_bins
    '''
    #These are the pixel indices of the potential void centres
    iterative_potential_voids=np.copy(potential_voids)
    void_centre_index=[]
    void_radii=[]
    #out_avg_dens=np.zeros((40,5))
    N=0
    #position of all potential centres
    #cenra=data[potential_voids,0]
    #cendec=data[potential_voids,1]
    #convert ra, dec to unit vector
    #cenvec=hp.ang2vec(cenra,cendec,lonlat=True)
    #convert ra, dec to pixel
    #cenpix=hp.ang2pix(nside,cenra,cendec,lonlat=True)
    use_delta_map=delta_map#*survey_mask

    non_mask_ind=np.arange(len(survey_mask))
    non_mask_ind=non_mask_ind[survey_mask!=0]
    while len(iterative_potential_voids)>0:
        index=int(iterative_potential_voids[0])
        #The positions of the iterative potential void centres
        #cenra_iter=data[iterative_potential_voids,0]
        #cendec_iter=data[iterative_potential_voids,1]
        #convert ra, dec to unit vector
        cenvec_iter=hp.pix2vec(nside,index,nest=nest)
        #convert ra, dec to pixel
        #cenpix_iter=hp.ang2pix(nside,cenra_iter,cendec_iter,lonlat=True)
        avg_dens=np.zeros(len(r_bins))
        tick=0
        for ii in range(len(r_bins)):
            ind_neighbours=hp.query_disc(nside,cenvec_iter,r_bins[ii],nest=nest)
            ind_neighbours_shell=hp.query_disc(nside,cenvec_iter,r_bins[ii]+shell_width,nest=nest)
            
            if len(ind_neighbours)==len(ind_neighbours_shell):
                if ii==0:
                    #average over the pixels
                    avg_dens[ii]=sum(use_delta_map[ind_neighbours])/float(sum(survey_mask[ind_neighbours]))
                elif ii>0:
                    avg_dens[ii]=avg_dens[ii-1]
            elif len(ind_neighbours_shell)>len(ind_neighbours):
                ring_ind=np.in1d(ind_neighbours_shell,ind_neighbours)
                if sum(survey_mask[ind_neighbours_shell][~ring_ind])!=0:
                    avg_dens[ii]=sum(use_delta_map[ind_neighbours_shell][~ring_ind])/float(sum(survey_mask[ind_neighbours_shell][~ring_ind]))
                else:
                    avg_dens[ii]=avg_dens[ii-1]
            if avg_dens[ii]>=tolerate:
                if (avg_dens[:ii]<tolerate).all():#==True:
                    #member pixel
                    void_members=np.copy(ind_neighbours)
                    #print((void_members==index).any())
                    void_radii_temp=r_bins[ii]+shell_width/2.
                    tick=1
            if tick==0:
                if ii==(len(r_bins)-1):
                    void_members=np.copy(ind_neighbours)
                    #print((void_members==index).any())
                    void_radii_temp=r_bins[ii]+shell_width/2.

        #find potential void centres pixels inside this void
        void_index=np.in1d(iterative_potential_voids,void_members)
        #print(len(cenpix_iter),len(void_index),len(iterative_potential_voids))
        #remove potential void centres
        iterative_potential_voids=iterative_potential_voids[~void_index]
        if N==0:
            void_centre_index=np.append(void_centre_index,index)
            void_radii=np.append(void_radii,void_radii_temp)
            out_avg_dens=np.copy(avg_dens)
        elif N>0:
            if overlap==False:
                #find existing void centres inside this void, so no void overlaps
                void_index2=np.in1d(void_centre_index,void_members)
            elif overlap==True:
                #set this overlap to all False, so ignores the overlap of void centres with existing voids
                void_index2=(np.zeros(len(void_centre_index))).astype(bool)
            #remove overlapping exisiting void centres
            void_centre_index=np.append(void_centre_index[~void_index2],index)
            #remove the void radii of the exisiting void centre
            void_radii=np.append(void_radii[~void_index2],void_radii_temp)
            #remove the avg_dens of the exisiting void centre
            if len(void_index2)==1:
                if void_index2==True:
                    out_avg_dens=np.copy(avg_dens)
                elif void_index2==False:
                    out_avg_dens=np.column_stack((out_avg_dens,avg_dens)) 
            elif len(void_index2)>1:
                out_avg_dens=np.column_stack((out_avg_dens[:,~void_index2],avg_dens))  
        if print_steps==True:
            print(len(iterative_potential_voids),len(void_centre_index))
            #print(void_centre_index)
            void_centre_index=void_centre_index.astype(int)
        N+=1
    print('Found %d voids.'%len(void_centre_index))
    
    #write_2Dvoid_file(data[void_centre_index,0],data[void_centre_index,1],void_radii, out_avg_dens,
                      #r_bins,tolerate,'gal',outdir,zslice)
    #return 0
    return void_centre_index,void_radii,out_avg_dens,N

def write_2Dvoid_file(void_centre_index, ra, dec, void_radii, out_avg_dens, r_bins, outdir):
    np.savetxt(outdir+'.txt', np.c_[void_centre_index, ra, dec, void_radii])
    print('written: '+outdir+'.txt')
    np.savetxt(outdir+'-profile.txt',np.c_[r_bins,out_avg_dens])
    print('written: '+outdir+'-profile.txt')
    
def inverse_ra(ra):
    inv_ra=np.copy(ra)
    inv_ind=ra>180
    inv_ra[inv_ind]=inv_ra[inv_ind]-360
    return inv_ra

#extract the profiles from the rand_cmb
def ISW_profile(ISW_map,rad_deg_bin,nside=512,nest=False):
    #pixels={}
    rad=rad_deg_bin*np.pi/180.
    ring=np.zeros(len(rad)-1)
    rad_deg=np.zeros(len(rad)-1)
    for kk in range(len(rad)-1):
        pixels1=hp.query_disc(nside,[1,0,0],rad[kk],nest=nest)
        pixels2=hp.query_disc(nside,[1,0,0],rad[kk+1],nest=nest)
        samepix=np.in1d(pixels2,pixels1)
        usepix=pixels2[~samepix]
        ring[kk]=np.mean(ISW_map[usepix])
        rad_deg[kk]=(rad_deg_bin[kk]+rad_deg_bin[kk+1])/2.
    return np.c_[rad_deg,ring]

def comp_filter_fixR(ISW_map,radius=4.):
    pixels=hp.query_disc(512,[1,0,0],radius*np.pi/180.,nest=False)
    circle=np.mean(ISW_map[pixels])*1e6

    pixels2=hp.query_disc(512,[1,0,0],radius*np.pi*np.sqrt(2.)/180.,nest=False)
    samepix=np.in1d(pixels2,pixels)
    pixels3=pixels2[~samepix]

    ring=np.mean(ISW_map[pixels3])*1e6

    return circle-ring

def compensated_filter(ISW_map,thetaR,nside=512,nest=False):
    thetaRs=np.sqrt(2)*thetaR
    deltaT=np.zeros(len(thetaR))
    for kk in range(len(thetaR)):
        pixels=hp.query_disc(nside,[1,0,0],thetaR[kk]*np.pi/180.,nest=nest)
        circle=np.mean(ISW_map[pixels])

        pixels2=hp.query_disc(nside,[1,0,0],thetaRs[kk]*np.pi/180.,nest=nest)
        samepix=np.in1d(pixels2,pixels)
        pixels3=pixels2[~samepix]

        ring=np.mean(ISW_map[pixels3])
        
        deltaT[kk]=circle-ring
    return deltaT

def generate_density_maps(ra, dec, Z, zlim, mask, vol_frac, outroot, nside=512, nest=False, sigma=20.):
    sel= (Z<zlim[1]) & (Z>zlim[0])
    meanz = np.mean(Z[sel])
    pixel_map=hp_pixelize(ra[sel],dec[sel],nside=nside,nest=nest,plots=0,title='')
    #method 1
    #mean=sum(pixel_map[non_zero_ind])/float(tot_pixels)
    #density_map=pixel_map/mean-1
    #density_map=density_map*use_mask
    #method 2
    A=np.copy(mask)
    non_zero_ind=mask!=0
    tot_pixels=sum(non_zero_ind)
    A[non_zero_ind]=pixel_map[non_zero_ind]/vol_frac[non_zero_ind]
    B=sum(A)/float(tot_pixels)
    density_map=A/B-1
    #smoothing at comoving sigma=10Mpc/h
    smoothed_density_map=hp.sphtfunc.smoothing(density_map*mask,sigma=sigma/uf.z_distance(meanz))
    #multiply the mask again
    map_out=smoothed_density_map*mask
    #write fits
    hp.fitsfunc.write_map(outroot,map_out,nest=False)
    print('Written: '+outroot)
    return map_out
    
def compensated_filter_scaled(ISW_map,mask,rbins,void_rr,nside=512,nest=False):
    #ISW_maps and masks are dictionaries of rotated maps
    sum_circle=np.zeros(len(rbins))
    sum_ring=np.zeros(len(rbins))
    #num_circle=np.zeros(len(rbins))
    #num_ring=np.zeros(len(rbins))
    
    for ii in range(len(void_rr)):
        thetaR=rbins*void_rr[ii]
        thetaRs=np.sqrt(2)*thetaR
        
        for kk in range(len(thetaR)):
            pixels=hp.query_disc(nside,[1,0,0],thetaR[kk]*np.pi/180.,nest=nest)
            sum_circle[kk]+=sum(ISW_map[ii][pixels])/float(sum(mask[ii][pixels]))
            #num_circle[kk]+=sum(mask[ii][pixels])

            pixels2=hp.query_disc(nside,[1,0,0],thetaRs[kk]*np.pi/180.,nest=nest)
            samepix=np.in1d(pixels2,pixels)
            pixels3=pixels2[~samepix]

            sum_ring[kk]+=sum(ISW_map[ii][pixels3])/float(sum(mask[ii][pixels3]))
            #num_ring[kk]+=sum(mask[ii][pixels3])
    #circle=sum_circle/num_circle.astype(float)
    #ring=sum_ring/num_ring.astype(float)
    deltaT=(sum_circle-sum_ring)/float(len(void_rr))
    return deltaT


def ISW_profile_scaled(ISW_map,mask,void_rr,rbins=np.linspace(0.2,3,20),nside=512):

    sum_ring=np.zeros(len(rbins))
    #num_ring=np.zeros(len(rbins))
    
    for ii in range(len(void_rr)):
        rad1=(rbins-0.1)*void_rr[ii]
        rad2=(rbins+0.1)*void_rr[ii]
        ring=np.zeros(len(rad1))

        for kk in range(len(rad1)):
            pixels1=hp.query_disc(nside,[1,0,0],rad1[kk],nest=False)
            pixels2=hp.query_disc(nside,[1,0,0],rad2[kk],nest=False)
            samepix=np.in1d(pixels2,pixels1)
            usepix=pixels2[~samepix]
            sum_ring[kk]+=sum(ISW_map[ii][usepix])/float(sum(mask[ii][usepix]))
            #num_ring[kk]+=sum(mask[ii][usepix]) 
    #ring=sum_ring/num_ring.astype(float)
    ring=sum_ring/float(len(void_rr))
    return np.c_[rbins,ring]


def from_delta_to_voids(mask, delta_map, ralim, declim, meanz, r_bins, outdir, nside=512, nest=False, density_cut=-0.3,edgelim=0.7,shell_width_cm=1.):
    pot_voids=potential_voids_indices(delta_map,density_cut=density_cut)
    
    #only look at ones inside the area
    potra, potdec = hp.pix2ang(nside, pot_voids, lonlat=True)
    ind1= (potra>ralim[0]) & (potra<ralim[1])
    ind2= (potdec>declim[0]) & (potdec<declim[1])
    ind=ind1*ind2
    pot_voids=pot_voids[ind]
    
    survey_mask=mask
    delta_map=delta_map
    potential_voids=pot_voids
    dd=uf.z_distance(meanz)
    theta_bins=r_bins/dd #in rad
    shell_width=shell_width_cm/dd
    
    #tol=np.mean(delta_map[mask.astype(bool)])
    tol=0
    
    void_centre_index,void_radii,out_avg_dens,N=simple_void_finder_2Dhealpy(survey_mask,delta_map,nside,nest,potential_voids,
                                                            theta_bins, shell_width,tolerate=tol,print_steps=False,overlap=True)
    ra, dec = hp.pix2ang(nside,void_centre_index.astype(int),lonlat=True)
    
    print('cutting edge...')
    num_rand=np.zeros(len(void_centre_index))
    for kk in range(len(void_centre_index)):
        void_cen_vec=hp.pix2vec(nside,void_centre_index[kk].astype(int))
        void_members=hp.query_disc(nside,void_cen_vec,void_radii[kk])
        num_rand[kk]=sum(mask[void_members])
        Npix=float(len(void_members))
        num_rand[kk]=num_rand[kk]/Npix
    edge_cut=num_rand>edgelim
    print('number of voids after cut: %s'%sum(edge_cut))
    
    void_centre_index=void_centre_index[edge_cut]
    ra=ra[edge_cut]
    dec=dec[edge_cut]
    void_radii=void_radii[edge_cut]
    out_avg_dens=out_avg_dens[:,edge_cut]
    
    write_2Dvoid_file(void_centre_index, ra, dec, void_radii, out_avg_dens, r_bins, outdir)

def cutting_egde_voids(void_file,rand_map,pixlim=400):
    void_cat=np.loadtxt(void_file)
    if void_cat.size==4:
        num_rand=0
        void_cen_vec=hp.pix2vec(512,void_cat[0].astype(int))
        void_members=hp.query_disc(512,void_cen_vec,void_cat[3])
        num_rand=sum(rand_map[void_members])
        Npix=float(len(void_members))
        num_rand=num_rand/Npix
    elif void_cat.size>4:
        num_rand=np.zeros(len(void_cat[:,0]))
        for kk in range(len(void_cat[:,0])):
            #find neighbours
            void_cen_vec=hp.pix2vec(512,void_cat[kk,0].astype(int))
            void_members=hp.query_disc(512,void_cen_vec,void_cat[kk,3])
            num_rand[kk]=sum(rand_map[void_members])
            Npix=float(len(void_members))
            num_rand[kk]=num_rand[kk]/Npix
    edge_cut=num_rand>pixlim
    return edge_cut

def save_cut_edge_voids(inroot, edge_cut, outroot):
    void_file=np.loadtxt(inroot+'.txt')
    void_profile=np.loadtxt(inroot+'-profile.txt')
    tick=0
    if void_file.size==4:
        if edge_cut==True:
            np.savetxt(outroot+'.txt',void_file)
            np.savetxt(outroot+'-profile.txt', void_profile)
        elif edge_cut==False:
            tick=1
    if void_file.size>4:
        if sum(edge_cut)==0:
            tick=1
        else:
            rr=void_profile[:,0]
            void_file_cut=void_file[edge_cut,:]
            void_profile_cut=np.copy(void_profile[:,1:])
            void_profile_cut=void_profile_cut[:,edge_cut]
            void_profile_cut=np.c_[rr, void_profile_cut]
            np.savetxt(outroot+'.txt',void_file_cut)
            np.savetxt(outroot+'-profile.txt', void_profile_cut)
    return tick

def group_voids(infile0, infile1, outfile):
    file0=np.loadtxt(infile0)
    file1=np.loadtxt(infile1)
    #fout=open(outfile,'w+')
    
    if file0.size>4:
        void_cen0=file0[:,0]
        void_rr0=file0[:,3]
        groups=np.zeros((len(void_cen0),2))
    elif file0.size==4:
        void_cen0=np.array([file0[0]])
        void_rr0=np.array([file0[3]])
        groups=np.zeros(2)
    
    print(len(void_cen0))
    if file1.size>4:
        void_cen1=file1[:,0]
        void_rr1=file1[:,3]
    elif file1.size==4:
        void_cen1=np.array([file1[0]])
        void_rr1=np.array([file1[3]])
    print(len(void_cen1))
    
    print((void_rr0.max()+void_rr1.max())/4.)
    largest_sep=(void_rr0.max()+void_rr1.max())/4.
    
    for kk in range(len(void_cen0)):
        if groups.size==2:
            groups[0]=void_cen0[kk]
        else:
            groups[kk,0]=void_cen0[kk]
        vec=hp.pix2vec(512,void_cen0[kk].astype(int))
        neighbours=hp.query_disc(512,vec,largest_sep)
        overlap=np.in1d(void_cen1,neighbours)
        sel_voids_cen=void_cen1[overlap]
        sel_voids_rr=void_rr1[overlap]
        ang0=hp.pix2ang(512,void_cen0[kk].astype(int),lonlat=True)
        #print(len(sel_voids))
        if len(sel_voids_cen)!=0:
            for vv in range(len(sel_voids_cen)):
                ang1=hp.pix2ang(512, sel_voids_cen[vv].astype(int),lonlat=True)
                d=hp.rotator.angdist(ang0,ang1,lonlat=True)
                dc=(void_rr0[kk]+sel_voids_rr[vv])/4.
                if d<=dc:
                    if groups.size==2:
                        groups[1]=sel_voids_cen[vv]
                    else:
                        groups[kk,1]=sel_voids_cen[vv]
    #write
    if groups.size==2:
        np.savetxt(outfile,np.c_[groups[0],groups[1]])
    else:
        np.savetxt(outfile,groups)
        #fout.write(str(group[kk].astype(int))[1:-1]+'\n')
    #fout.close()
    print('written: '+outfile)
    return 0

def p_value_2_sigma(p_value):
    import scipy.special as special
    xx=np.linspace(0,10,1000)
    yy=special.erfc(xx/np.sqrt(2))
    #pl.plot(xx,yy)
    sortind=np.argsort(yy)
    sigma=np.interp(p_value,yy[sortind],xx[sortind])
    return sigma

def cumulative_s2n(covariance, signal):
    ddof=np.zeros(len(signal))
    s2n=np.zeros(len(signal))
    for ii in range(len(signal)):
        use_cov=covariance[:ii+1,:ii+1]
        invcov=np.linalg.inv(use_cov)
        use_sig=signal[:ii+1]
        A=np.dot(use_sig,invcov)
        chi2_signal=np.dot(use_sig,A)
        ddof[ii]=ii+1
        import scipy.stats as stats
        p_value = 1 - stats.chi2.cdf(x=chi2_signal,df=ddof[ii])
        s2n[ii]=p_value_2_sigma(p_value)
        #s2n[ii]=chi2_signal/float(ii+1)
    return ddof, s2n

def rot_cmb_at_voids_pos(hmap,mask,ra,dec):
    Rot_maps={}
    Rot_mask={}

    for ii in range(len(ra)):
        rot_theta=dec[ii]
        rot_phi=ra[ii]
        if rot_phi>180:
            rot_phi=rot_phi-360
        #print(rot_theta,rot_phi,voidra[ii])
        rot_map=rotate_map(hmap, rot_theta, rot_phi, nest=False)
        Rot_maps[ii]=rot_map

        #rotate the mask
        rot_mask=rotate_map(mask,rot_theta, rot_phi, nest=False)
        Rot_mask[ii]=rot_mask
    
    return Rot_maps, Rot_mask

def rot_cmb_at_voids_pos_cutdisc(hmap,mask,ra,dec,cutrad=15,nside=512):
    #cutrad in degrees
    vec=np.array([ 1., 0.,  0.]) #ra=dec=0
    radius=cutrad/180.*np.pi
    useindex=hp.query_disc(nside,vec,radius)
    
    Rot_maps=np.zeros((len(useindex),len(ra)))
    Rot_mask=np.zeros((len(useindex),len(ra)))
    for ii in range(len(ra)):
        rot_theta=dec[ii]
        rot_phi=ra[ii]
        if rot_phi>180:
            rot_phi=rot_phi-360
        #print(rot_theta,rot_phi,voidra[ii])
        rot_map=rotate_map(hmap, rot_theta, rot_phi, nest=False)
        Rot_maps[:,ii]=rot_map[useindex]

        #rotate the mask
        rot_mask=rotate_map(mask,rot_theta, rot_phi, nest=False)
        Rot_mask[:,ii]=rot_mask[useindex]
    
    return useindex,Rot_maps, Rot_mask

def stacking_voids_scaled(ra,dec,voidrr,mapin,maskin,L=120,maxR=3.,plot=0):
    '''
    void radius is in radian, then converted to degrees
    '''
    out_data=np.zeros((L**2,len(voidrr)))
    out_mask=np.zeros((L**2,len(voidrr)))
    for ii in range(len(voidrr)):
        #smoothed_cmbmap=hp.sphtfunc.smoothing(rot_cmbTmaps[ii],fwhm=sigma*np.pi/180.,verbose=False)
        rot_theta=dec[ii]
        rot_phi=ra[ii]
        rv=voidrr[ii]*180./np.pi
        if rot_phi>180:
            rot_phi=rot_phi-360
        rot_map=rotate_map(mapin, rot_theta, rot_phi, nest=False)
        rot_mask=rotate_map(maskin, rot_theta, rot_phi, nest=False)
        
        bincentre=np.linspace(-maxR,maxR,L)*rv
        gridy=np.outer(bincentre,np.ones(L)) #dec
        gridx=np.outer(np.ones(L),bincentre) #ra
        #lonlat=True
        #pixels=hp.ang2pix(512, gridx.flatten(), gridy.flatten(), nest=False, lonlat=lonlat)
        values=hp.pixelfunc.get_interp_val(rot_map, gridx.flatten(), gridy.flatten(), nest=False, lonlat=True)
        out_data[:,ii]=values
        
        values=hp.pixelfunc.get_interp_val(rot_mask, gridx.flatten(), gridy.flatten(), nest=False, lonlat=True)
        out_mask[:,ii]=values
        
    #values=values.reshape(L,L)
    #values_mask=values_mask.reshape(L,L)
    if plot==1:
        pl.imshow((values/values_mask)*1e6,origin='lower',cmap='RdBu',extent=[-maxR,maxR,-maxR,maxR],
                  vmax=11.5,vmin=-11.5)
        pl.colorbar()
        pl.plot([0,0],[0,0],'w+')
        x,y1,y2=circles(0,0,1)
        pl.plot(x,y1,'k-')
        pl.plot(x,y2,'k-')
        x,y1,y2=circles(0,0,np.sqrt(2))
        pl.plot(x,y1,'k-')
        pl.plot(x,y2,'k-')
    return out_data, out_mask

def scaled_stack_profile(scaled_stack,rad_deg_bins,maxR,L):
    '''scaled_stack is in 2D'''
    bincentre=np.linspace(-maxR,maxR,L)
    gridy=np.outer(bincentre,np.ones(L))
    gridx=np.outer(np.ones(L),bincentre)
    dT=np.zeros(len(rad_deg_bins)-1)
    radx=np.zeros(len(rad_deg_bins)-1)
    for ii in range(len(rad_deg_bins)-1):
        ind1=(gridy**2+gridx**2)>=(rad_deg_bins[ii])**2
        ind2=(gridy**2+gridx**2)<=(rad_deg_bins[ii+1])**2
        dT[ii]=np.mean(scaled_stack[ind1*ind2])
        radx[ii]=(rad_deg_bins[ii]+rad_deg_bins[ii+1])/2.
    return np.c_[radx,dT]

#=======Cluster functions=======#
def potential_clusters_indices(delta,density_cut=0.3):
    '''
    delta: of all data
    density_cut: to determine the potential clusters
    return: indices of potential voids, sorted according to delta
    '''
    deltaindex=np.arange(len(delta))
    potential_cluster=delta>density_cut
    potential_cluster=deltaindex[potential_cluster]
    sortidx_pot_cluster=(np.argsort(delta[potential_cluster]))[::-1] #from largest to smallest
    sorted_potential_cluster=potential_cluster[sortidx_pot_cluster]
    return sorted_potential_cluster


def simple_cluster_finder_2Dhealpy(survey_mask,delta_map,nside,nest,potential_clusters,r_bins, shell_width,tolerate=0,print_steps=True,overlap=True):
    '''
    data: [ra, dec]
    pixel_map: healpix map of galaxies
    delta_map: healpix map of sum of delta of galaxies in each pixel
    potential_voids: indices of the potential clusters, sorted according to delta
    r_bins: radius in radian to look at
    return: void centres, radii, first 5 avg_dens as a function of r_bins
    '''
    #These are the pixel indices of the potential void centres
    iterative_potential_cluster=np.copy(potential_clusters)
    cluster_centre_index=[]
    cluster_radii=[]
    #out_avg_dens=np.zeros((40,5))
    N=0
    #position of all potential centres
    #cenra=data[potential_voids,0]
    #cendec=data[potential_voids,1]
    #convert ra, dec to unit vector
    #cenvec=hp.ang2vec(cenra,cendec,lonlat=True)
    #convert ra, dec to pixel
    #cenpix=hp.ang2pix(nside,cenra,cendec,lonlat=True)
    use_delta_map=delta_map#*survey_mask

    non_mask_ind=np.arange(len(survey_mask))
    non_mask_ind=non_mask_ind[survey_mask!=0]
    while len(iterative_potential_cluster)>0:
        index=int(iterative_potential_cluster[0])
        #The positions of the iterative potential void centres
        #cenra_iter=data[iterative_potential_voids,0]
        #cendec_iter=data[iterative_potential_voids,1]
        #convert ra, dec to unit vector
        cenvec_iter=hp.pix2vec(nside,index,nest=nest)
        #convert ra, dec to pixel
        #cenpix_iter=hp.ang2pix(nside,cenra_iter,cendec_iter,lonlat=True)
        avg_dens=np.zeros(len(r_bins))
        tick=0
        for ii in range(len(r_bins)):
            ind_neighbours=hp.query_disc(nside,cenvec_iter,r_bins[ii],nest=nest)
            ind_neighbours_shell=hp.query_disc(nside,cenvec_iter,r_bins[ii]+shell_width,nest=nest)

            if len(ind_neighbours)==len(ind_neighbours_shell):
                if ii==0:
                    #average over the pixels
                    avg_dens[ii]=sum(use_delta_map[ind_neighbours])/float(sum(survey_mask[ind_neighbours]))
                elif ii>0:
                    avg_dens[ii]=avg_dens[ii-1]
            elif len(ind_neighbours_shell)>len(ind_neighbours):
                ring_ind=np.in1d(ind_neighbours_shell,ind_neighbours)
                if sum(survey_mask[ind_neighbours_shell][~ring_ind])!=0:
                    avg_dens[ii]=sum(use_delta_map[ind_neighbours_shell][~ring_ind])/float(sum(survey_mask[ind_neighbours_shell][~ring_ind]))
                else:
                    avg_dens[ii]=avg_dens[ii-1]
            if avg_dens[ii]<=tolerate:
                if (avg_dens[:ii]>tolerate).all():#==True:
                    #member pixel
                    cluster_members=np.copy(ind_neighbours)
                    #print((void_members==index).any())
                    cluster_radii_temp=r_bins[ii]+shell_width/2.
                    tick=1
            if tick==0:
                if ii==(len(r_bins)-1):
                    cluster_members=np.copy(ind_neighbours)
                    #print((void_members==index).any())
                    cluster_radii_temp=r_bins[ii]+shell_width/2.

        #find potential void centres pixels inside this void
        cluster_index=np.in1d(iterative_potential_cluster,cluster_members)
        #print(len(cenpix_iter),len(void_index),len(iterative_potential_voids))
        #remove potential void centres
        iterative_potential_cluster=iterative_potential_cluster[~cluster_index]
        if N==0:
            cluster_centre_index=np.append(cluster_centre_index,index)
            cluster_radii=np.append(cluster_radii,cluster_radii_temp)
            out_avg_dens=np.copy(avg_dens)
        elif N>0:
            if overlap==False:
                #find existing void centres inside this void, so no void overlaps
                cluster_index2=np.in1d(cluster_centre_index,cluster_members)
            elif overlap==True:
                #set this overlap to all False, so ignores the overlap of void centres with existing voids
                cluster_index2=(np.zeros(len(cluster_centre_index))).astype(bool)
            #remove overlapping exisiting void centres
            cluster_centre_index=np.append(cluster_centre_index[~cluster_index2],index)
            #remove the void radii of the exisiting void centre
            cluster_radii=np.append(cluster_radii[~cluster_index2],cluster_radii_temp)
            #remove the avg_dens of the exisiting void centre
            if len(cluster_index2)==1:
                if cluster_index2==True:
                    out_avg_dens=np.copy(avg_dens)
                elif cluster_index2==False:
                    out_avg_dens=np.column_stack((out_avg_dens,avg_dens))
            elif len(cluster_index2)>1:
                out_avg_dens=np.column_stack((out_avg_dens[:,~cluster_index2],avg_dens))
        if print_steps==True:
            print(len(iterative_potential_cluster),len(cluster_centre_index))
            #print(void_centre_index)
            cluster_centre_index=cluster_centre_index.astype(int)
        N+=1
    print('Found %d clusters.'%len(cluster_centre_index))
#write_2Dvoid_file(data[void_centre_index,0],data[void_centre_index,1],void_radii, out_avg_dens,
                      #r_bins,tolerate,'gal',outdir,zslice)
    #return 0
    return cluster_centre_index,cluster_radii,out_avg_dens,N

def from_delta_to_clusters(mask, delta_map, ralim, declim, meanz, r_bins, outdir, nside=512, nest=False, density_cut=0.3):
    pot_clusters=potential_clusters_indices(delta_map,density_cut=density_cut)

    #only look at ones inside the area
    potra, potdec = hp.pix2ang(512, pot_clusters, lonlat=True)
    ind1= (potra>ralim[0]) & (potra<ralim[1])
    ind2= (potdec>declim[0]) & (potdec<declim[1])
    ind=ind1*ind2
    pot_clusters=pot_clusters[ind]

    survey_mask=mask
    delta_map=delta_map
    potential_clusters=pot_clusters
    dd=uf.z_distance(meanz)
    theta_bins=r_bins/dd #in rad
    shell_width=1./dd

    #tol=np.mean(delta_map[mask.astype(bool)])
    tol=0

    cluster_centre_index,cluster_radii,out_avg_dens,N=simple_cluster_finder_2Dhealpy(survey_mask,delta_map,nside,nest,potential_clusters,
                                                            theta_bins, shell_width,tolerate=tol,print_steps=False,overlap=True)
    ra, dec = hp.pix2ang(512,cluster_centre_index.astype(int),lonlat=True)
    write_2Dvoid_file(cluster_centre_index, ra, dec, cluster_radii, out_avg_dens, r_bins, outdir)

def dump_save(stuff,filename):
    '''This saves the dictionary and loads it from appropriate files'''
    with open(filename,'wb') as fout:
        pickle.dump(stuff,fout,pickle.HIGHEST_PROTOCOL)
        #json.dump(self.impute, fout, sort_keys=True, indent=3)
    print('written impute ditionary:',filename)
    return 0
def dump_load(filename):
    with open(filename,'rb') as fin:
        stuff=pickle.load(fin)
        #self.impute = json.load(fin)
    #print('loaded impute ditionary:',filename)
    return stuff


def DES_like_coloursel(xcoord,ycoord,zcoord):
    #we put both gr-rz selection and gw1-rz selection
    colour_sel=dump_load('/disk2/qhang/ISW_CMB/DECALS/RedMaGiC-like-coloursel-gr-rz.pkl')
    
    x_range=colour_sel['x_range']
    y_range=colour_sel['y_range']
    bins=colour_sel['bins']
    grid=colour_sel['grid']

    xbinedge=np.linspace(x_range[0],x_range[1],bins[0]+1)
    xbins=np.digitize(xcoord,xbinedge)
    ind1=(xbins>0)&(xbins<=bins[0])

    ybinedge=np.linspace(y_range[0],y_range[1],bins[1]+1)
    ybins=np.digitize(ycoord,ybinedge)
    ind2=(ybins>0)&(ybins<=bins[1])

    ind3=ind1*ind2
    colourind_gr=grid[xbins[ind3]-1,ybins[ind3]-1]
    colourind_gr=colourind_gr.astype(bool)
    
    #convert to number index
    sel_gr=np.arange(len(xcoord))
    sel_gr=sel_gr[ind3][colourind_gr]
    
    colour_sel=dump_load('/disk2/qhang/ISW_CMB/DECALS/RedMaGiC-like-coloursel-gw1-rz.pkl')
    
    z_range=colour_sel['x_range']
    y_range=colour_sel['y_range']
    bins=colour_sel['bins']
    grid=colour_sel['grid']
    
    #x is gw1
    zbinedge=np.linspace(z_range[0],z_range[1],bins[0]+1)
    zbins=np.digitize(zcoord,zbinedge)
    ind4=(zbins>0)&(zbins<=bins[0])

    ybinedge=np.linspace(y_range[0],y_range[1],bins[1]+1)
    ybins=np.digitize(ycoord,ybinedge)
    ind5=(ybins>0)&(ybins<=bins[1])

    ind6=ind4*ind5
    colourind_gw1=grid[zbins[ind6]-1,ybins[ind6]-1]
    colourind_gw1=colourind_gw1.astype(bool)
    
    #convert to number index
    sel_gw1=np.arange(len(xcoord))
    sel_gw1=sel_gw1[ind6][colourind_gw1]
    
    #combine
    sel=np.intersect1d(sel_gr,sel_gw1)

    return sel

def find_delta_c(delta_dist, pc):
    y=np.cumsum(delta_dist,dtype=float)
    y=y/y[-1]
    ind=np.where(y<=pc)[0]
    return ind[-1]


########Rotation and stacking related codes

def read_rotmaps(outdir,zbin=0,map_type='densmap',index=True):
    for ii in range(1,9):
        fin=dump_load(outdir+'%s-bin-%d-rot-run.%d.pkl'%(map_type,zbin,ii))
        if fin['rotmap'].size!=0:
            if ii==1:
                dataholder_map=fin['rotmap']
                dataholder_mask=fin['rotmask']
            else:
                dataholder_map=np.column_stack((dataholder_map,fin['rotmap']))
                dataholder_mask=np.column_stack((dataholder_mask,fin['rotmask']))
                
    print(dataholder_map.shape,dataholder_mask.shape)
    if index==True:
        useindex=fin['useindex']
        return dataholder_map, dataholder_mask, useindex
    else:
        return dataholder_map, dataholder_mask

#compute Jackknife error
def jackknife_error_stack(dataholder_map, dataholder_mask, useindex, nside, rad_deg):
    ndata=len(dataholder_map[0,:])
    jackknife_matrix=np.zeros(dataholder_map.shape)
    index=np.arange(ndata)
    jackknife_profile=np.zeros((len(rad_deg)-1,ndata+2))
    #or maybe can use broadcast ###--memory error.
    mask=np.identity(ndata)
    mask=(mask-1)*(-1)
    #product_map=dataholder_map[:,:,None]*mask
    if (ndata>210)&(ndata<300):
        half_ind=len(useindex)/3#half pixels a time
        s1=np.sum(dataholder_map[:half_ind,:,None]*mask[None,:,:],axis=1)
        s2=np.sum(dataholder_map[half_ind:2*half_ind,:,None]*mask[None,:,:],axis=1)
        s3=np.sum(dataholder_map[2*half_ind:,:,None]*mask[None,:,:],axis=1)
        jk_map=np.append(s1,s2,axis=0)
        jk_map=np.append(jk_map,s3,axis=0)
        
        s1=np.sum(dataholder_mask[:half_ind,:,None]*mask[None,:,:],axis=1)
        s2=np.sum(dataholder_mask[half_ind:2*half_ind,:,None]*mask[None,:,:],axis=1)
        s3=np.sum(dataholder_mask[2*half_ind:,:,None]*mask[None,:,:],axis=1)
        jk_mask=np.append(s1,s2,axis=0) 
        jk_mask=np.append(jk_mask,s3,axis=0)
        
        jk_mask[jk_mask==0]=1.
        jackknife_matrix=jk_map/jk_mask
    
    if ndata<210:
        jk_map=np.sum(dataholder_map[:,:,None]*mask[None,:,:],axis=1) 
        #product_mask=dataholder_mask[:,:,None]*mask
        jk_mask=np.sum(dataholder_mask[:,:,None]*mask[None,:,:],axis=1)
         
        jk_mask[jk_mask==0]=1.
        jackknife_matrix=jk_map/jk_mask
    
    if (ndata>=300):
        #do it in the slow way, loop over all jackknife samples
        for ii in range(ndata):
            if ii==0:
                s1=np.sum(dataholder_map[:,ii+1:],axis=1)
                s2=np.sum(dataholder_mask[:,ii+1:],axis=1)
            if (ii>0)&(ii<(ndata-1)):
                s1=np.sum(dataholder_map[:,:ii],axis=1)+np.sum(dataholder_map[:,ii+1:],axis=1)
                s2=np.sum(dataholder_mask[:,:ii],axis=1)+np.sum(dataholder_mask[:,ii+1:],axis=1)
            if ii==(ndata-1):
                s1=np.sum(dataholder_map[:,:ii],axis=1)
                s2=np.sum(dataholder_mask[:,:ii],axis=1)
            s2[s2==0]=1.
            jackknife_matrix[:,ii]=s1/s2
    
    #profile indices here
    #pixels={}
    rad=rad_deg*np.pi/180.
    radx=np.zeros(len(rad)-1)
    
    #print('finish1')
    for kk in range(len(rad)-1):
        pixels1=hp.query_disc(nside,[1,0,0],rad[kk],nest=False)
        pixels2=hp.query_disc(nside,[1,0,0],rad[kk+1],nest=False)
        samepix=np.in1d(pixels2,pixels1)
        usepix=pixels2[~samepix]
        indd2=np.in1d(useindex,usepix)
        jackknife_profile[kk,2:]=np.mean(jackknife_matrix[indd2,:],axis=0)
        radx[kk]=(rad_deg[kk]+rad_deg[kk+1])/2.
    #print('finish2')
        
    #now compute the mean and std of the profile, also return the profile matrix to compute covariance later
    mean=np.mean(jackknife_profile[:,2:],axis=1)
    std=np.std(jackknife_profile[:,2:],axis=1)
    jackknife_profile[:,0]=mean
    jackknife_profile[:,1]=std
    profile_out=jackknife_profile[:,:2]
    covariance_out=np.cov(jackknife_profile[:,2:])*(ndata-1)
    return radx,profile_out,covariance_out

#for the scaled case, the profile extraction part is different
def jackknife_error_stack_scaled(dataholder_map, dataholder_mask, rad_deg_scaled, L=200,maxR=3.):
    ndata=len(dataholder_map[0,:])
    jackknife_matrix=np.zeros(dataholder_map.shape)
    index=np.arange(ndata)
    jackknife_profile=np.zeros((len(rad_deg_scaled)-1,ndata+2))
    #or maybe can use broadcast ###--memory error.
    mask=np.identity(ndata)
    mask=(mask-1)*(-1)
    #product_map=dataholder_map[:,:,None]*mask
    if (ndata>=210)&(ndata<300):
        half_ind=int(L**2/3)#half pixels a time
        s1=np.sum(dataholder_map[:half_ind,:,None]*mask[None,:,:],axis=1)
        s2=np.sum(dataholder_map[half_ind:2*half_ind,:,None]*mask[None,:,:],axis=1)
        s3=np.sum(dataholder_map[2*half_ind:,:,None]*mask[None,:,:],axis=1)
        jk_map=np.append(s1,s2,axis=0)
        jk_map=np.append(jk_map,s3,axis=0)
        
        s1=np.sum(dataholder_mask[:half_ind,:,None]*mask[None,:,:],axis=1)
        s2=np.sum(dataholder_mask[half_ind:2*half_ind,:,None]*mask[None,:,:],axis=1)
        s3=np.sum(dataholder_mask[2*half_ind:,:,None]*mask[None,:,:],axis=1)
        jk_mask=np.append(s1,s2,axis=0) 
        jk_mask=np.append(jk_mask,s3,axis=0)
        
        jk_mask[jk_mask==0]=1.
        jackknife_matrix=jk_map/jk_mask
            
    if ndata<210:
        jk_map=np.sum(dataholder_map[:,:,None]*mask[None,:,:],axis=1) 
        #product_mask=dataholder_mask[:,:,None]*mask
        jk_mask=np.sum(dataholder_mask[:,:,None]*mask[None,:,:],axis=1)
    
        jk_mask[jk_mask==0]=1.
        jackknife_matrix=jk_map/jk_mask
    
    if (ndata>=300):
        #do it in the slow way, loop over all jackknife samples
        for ii in range(ndata):
            if ii==0:
                s1=np.sum(dataholder_map[:,ii+1:],axis=1)
                s2=np.sum(dataholder_mask[:,ii+1:],axis=1)
            if (ii>0)&(ii<(ndata-1)):
                s1=np.sum(dataholder_map[:,:ii],axis=1)+np.sum(dataholder_map[:,ii+1:],axis=1)
                s2=np.sum(dataholder_mask[:,:ii],axis=1)+np.sum(dataholder_mask[:,ii+1:],axis=1)
            if ii==(ndata-1):
                s1=np.sum(dataholder_map[:,:ii],axis=1)
                s2=np.sum(dataholder_mask[:,:ii],axis=1)
            s2[s2==0]=1.
            jackknife_matrix[:,ii]=s1/s2
    
    bincentre=np.linspace(-maxR,maxR,L)
    gridy=np.outer(bincentre,np.ones(L))
    gridx=np.outer(np.ones(L),bincentre)
    radx=np.zeros(len(rad_deg_scaled)-1)
    xx=gridx.flatten()
    yy=gridy.flatten()
    
    for kk in range(len(rad_deg_scaled)-1):
        ind1=(yy**2+xx**2)>=(rad_deg_scaled[kk])**2
        ind2=(yy**2+xx**2)<=(rad_deg_scaled[kk+1])**2
        indd2=ind1*ind2
        radx[kk]=(rad_deg_scaled[kk]+rad_deg_scaled[kk+1])/2.
        jackknife_profile[kk,2:]=np.mean(jackknife_matrix[indd2,:],axis=0)
    #print('finish2')

    #now compute the mean and std of the profile, also return the profile matrix to compute covariance later
    mean=np.mean(jackknife_profile[:,2:],axis=1)
    std=np.std(jackknife_profile[:,2:],axis=1)
    jackknife_profile[:,0]=mean
    jackknife_profile[:,1]=std
    profile_out=jackknife_profile[:,:2]
    covariance_out=np.cov(jackknife_profile[:,2:])*(ndata-1)
    return radx,profile_out,covariance_out

def stack_map(dataholder_map, dataholder_mask, stack_all=True,stack_index=None):
    #if stack_index='all', stack everything, otherwise supply the index for stacking
    
    #now stack
    if stack_all==True:
        sum_map=np.sum(dataholder_map,axis=1)
        sum_mask=np.sum(dataholder_mask,axis=1)
    else:
        sum_map=np.sum(dataholder_map[:,stack_index],axis=1)
        sum_mask=np.sum(dataholder_mask[:,stack_index],axis=1)
    
    #print(np.mean(sum_mask),sum_mask.max())
    sum_mask[sum_mask==0]=1.
    
    density_stack=sum_map/sum_mask
        
    return density_stack

def convert_stack_to_map(nside,useindex,stack):
    mapout=np.zeros(12*nside**2)
    mapout[useindex]=stack
    return mapout

def vec2pix_func(x,y,z):   
    return hp.vec2pix(512,x,y,z)