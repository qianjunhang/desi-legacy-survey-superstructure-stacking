# DESI Legacy Survey superstructure stacking

This repository contains the source code used to perform the analysis presented in Hang et al. (2021) ([arxiv:2105.11936](https://arxiv.org/abs/2105.11936)).
If you have questions/queries then contact qhang@roe.ac.uk or hangqianjun007@gmail.com.

## Main data products:
* Void and cluster centres:\
files: [data/LegacySurvey/voids-smth-20](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/tree/master/data/LegacySurvey/voids-smth-20), [data/LegacySurvey/clusters-smth-20](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/tree/master/data/LegacySurvey/clusters-smth-20)\
<b>Description:</b> These are the superstructure catalogue used in the analysis. The files include information on the pixel number, ra, and dec for the superstructure centres, and the size of the superstructure in radian. These files can also be produced using the codes in the [example Jupyter notebook](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/jupyternotebook/Legacy_survey_voids.ipynb).

* Mock catalogue from the Multidark Simulation:
    - Galaxy catalogue with photo-z: [lc_mbox_combined_w_photoz.fits](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/lc_mbox_combined_w_photoz.fits).
    - Galaxy density maps: [bin0](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/galmap-photozsel-bin-0.fits), [bin1](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/galmap-photozsel-bin-1.fits), [bin2](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/galmap-photozsel-bin-2.fits), [bin3](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/galmap-photozsel-bin-3.fits).
    - Lensing convergence map: [kappa_map_gzMDPL2_DM_None_nside512_nested.pkl](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/kappa_map_gzMDPL2_DM_None_nside512_nested.pkl). Notice that the maps are in NESTED ordering. To convert to RING ordering, use healpy.reorder function.
    - ISW map: [bin0](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/InterN500Nr256R100.834dR2.00.fits), [bin1](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/InterN500Nr256R834.1203dR2.00.fits), [bin2](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/InterN500Nr256R1203.1540dR2.00.fits), [bin3](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/InterN500Nr256R1540.1944dR2.00.fits). Note the maps are made with nside=256.\
<b>Description:</b> This is the mock catalogue used for the analysis. We make use of the MultiDark Planck (MDPL2; Klypin et al. 2016) simulations with Planck 2013 Cosmology. The simulation is publicly available through the [CosmoSim database](https://www.cosmosim.org/cms/simulations/mdpl2/). For details of HOD model, lightcone construction, and photometric redshifts, please see Section 2.3 in Hang et al. (2021).


* Generating lightcone galaxy and kappa map:
   - The lightcone galaxy and dark-matter density can be generated using [lccode](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/functions/lc_utility.py) and the lensing convergence map from the the dark matter light-cone can be generated using [kappa-code](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/functions/kapp_utility.py). The raw simulations data is publicly available on the cosmosim data base directly. We do provide the [dark-matter density map in shells](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/Multidarkmock/densmap_nside512_w1/) which was used to generate convergence maps.

* Generating ISW map:
   - The code can be found [here](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/tree/master/functions/Generate%20ISW%20maps%20from%20N-body%20simulations). Notice that the code uses Fortran and IDL.

All maps are stored as healpix maps with nside=512 and RING ordering, unless otherwise stated.

## Analysis pipeline
The pipeline requires the following python packages:\
    - healpy\
    - fitsio
Functions used for the analysis can be found in the [functions](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/tree/master/functions) folder.

The [example Jupyter notebook](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/jupyternotebook/Legacy_survey_voids.ipynb) gives a walkthrough exmaple for superstructure finding and stacking processes. The DESI Legacy Survey galaxy density maps are provided [here](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/LegacySurvey/densmap_star.pkl) along with the survey [mask](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/LegacySurvey/Legacy_footprint_completeness_mask_128-withcuts.fits). These maps are produced in Hang et al. (2021) ([arxiv:2010.00466](https://arxiv.org/abs/2010.00466)) and have been corrected by systematics (survey completeness and stellar density). If you would like to access the pipeline for making these maps, visit [https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/](https://gitlab.com/qianjunhang/desi-legacy-survey-cross-correlations/). The void-finding algorithm follows the description in S\'anchez et al. (2017).

The covariance matrix from 1000 random void/cluster positions for the CMB temperature map stacking is provided in [random-clusters-stacked_profile-mean-std-cov.txt](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/LegacySurvey/random-clusters-stacked_profile-mean-std-cov.txt) and [random-voidss-stacked_profile-mean-std-cov.txt](https://gitlab.com/qianjunhang/desi-legacy-survey-superstructure-stacking/-/blob/master/data/LegacySurvey/random-voids-stacked_profile-mean-std-cov.txt).

If you use the code or data products on this site, please cite Hang et al. (2021) ([arxiv:2105.11936](https://arxiv.org/abs/2105.11936)).


